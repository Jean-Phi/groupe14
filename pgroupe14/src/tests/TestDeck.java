package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import models.Deck;
import models.Question;
import test.Explorateur;

public class TestDeck {

	private static Deck deck;
	private List<Question> questions;
	private Question q;

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		deck = Deck.getInstance();
		questions=(List<Question>) Explorateur.getField(deck, "questions");
		q=new Question("Bilal","Manga","FLUBER");
		q.addClue("1");
		q.addClue("2");
		q.addClue("3");
		questions.add(q);
	}

	@After
	public void tearDown() throws Exception {
		questions.clear();
		deck = null;
		questions=null;
		q=null;
	}

	@Test
	public void addQuestion() {
		Question q = new Question("Bilal", "Manga", "Mister 3");
		deck.addQuestion(q);
		assertEquals("Test de addQuestion()", questions.get(1), q);
		assertEquals("Test de addQuestion()", questions.size(), 2);
	}
	@Test
	public void getQuestion(){
		assertEquals("Test de getQuestion()",deck.getQuestion(0), q);
	}
	
	@Test
	public void removeQuestion(){
		deck.removeQuestion(q);
		assertEquals("Test de removeQuestion()",questions.size(),0);
	}
	
	@Test
	public void getDeckTheme(){
		List<Question> liste = new ArrayList<>();
		liste.add(q);
		assertEquals("Test de getDeckTheme()",deck.getDeckTheme("Manga"),liste);
	}
	
	@Test
	public void setQuestion(){
		Question q=new Question("author", "theme", "answer");
		deck.setQuestion(0, q);
		assertEquals("Test de setQuestion()",questions.get(0),q);
	}
	
	@Test
	public void getThemes(){
		List<String> liste = new ArrayList<>();
		liste.add("Manga");
		assertEquals("Test de getDeckTheme()",deck.getThemes(),liste);
	}

}
