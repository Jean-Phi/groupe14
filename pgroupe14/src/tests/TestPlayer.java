package tests;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import models.User;
//import test.Explorateur;

public class TestPlayer {
	private static User player;
	//private static Integer score;
	
	@Before
	public void setUp() throws Exception {
		player=new User("Lol","Lol");
		player.setScore(5);
		//score=(Integer)Explorateur.getField(player, "score");
		//score=5;
	}

	@After
	public void tearDown() throws Exception {
		player=null;
		//score=null;
	}

	@Test
	public void testResetScore() {
		player.resetScore();
		//System.out.println(player.getScore());
		//assertEquals("test de resetScore()", 0, (int)score);
		assertEquals("test de resetScore()", 0, player.getScore());
	}
	
	@Test
	public void testAddScore() {
		player.addPoint();
		//System.out.println(player.getScore());
		//assertEquals("test de addPoint()", 6, (int)score);
		assertEquals("test de addPoint()", 6, player.getScore());
	}
	

}
