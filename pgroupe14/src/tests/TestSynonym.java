package tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import test.Explorateur;
import utilities.Synonym;


public class TestSynonym {
	private static Synonym synonym;
	private static List<List<String>> dictionary;


	@Before
	public void setUp() throws Exception {
		synonym = Synonym.getInstance();
		dictionary=(List<List<String>>) Explorateur.getField(synonym, "dictionary");
		ArrayList<String> data= new ArrayList<>();
		data.add("un"); data.add("one"); data.add("1");
		dictionary.add(data);
		data=null;
	}

	@After
	public void tearDown() throws Exception {
		synonym = null;
		dictionary.remove(0);
		dictionary=null;
	}

	@Test 
	public void addSynonym(){
		List<String> data= new ArrayList<>();
		data.add("deux"); data.add("twoo"); data.add("2");
		synonym.addSynonym(data);
		assertEquals("Test add() size",synonym.size(), dictionary.size());
		assertEquals("Test add() get",dictionary.get(1), data);
		dictionary.remove(1);
	}
	@Test 
	public void removeSynonym(){
		List<String> data= new ArrayList<>();
		data.add("deux"); data.add("twoo"); data.add("2");
		dictionary.add(data);
		synonym.removeSynonym(0);
		assertEquals("Test remove()",synonym.size(), dictionary.size());
	}
	@Test
	public void replaceSynonym(){
		assertEquals("Test replaceSynonym()","un", Synonym.replaceSynonym("one"));
	}
	@Test
	public void testToString(){
		String resultat = "Synonym :\nun, one, 1, \n" ;
		assertEquals("Test de toString()",synonym.toString(),resultat);

	}

}


