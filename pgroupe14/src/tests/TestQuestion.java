package tests;

import static org.junit.Assert.*;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import models.Question;
import test.Explorateur;

public class TestQuestion {

	private static Question q;
	private static List<String> clues;
	
	@SuppressWarnings("unchecked")
	@Before
	public void setUp() throws Exception {
		q = new Question("Alexandre", "Calcul", "2");
		clues = (List<String>) Explorateur.getField(q, "clues");
		clues.add("Je suis divisible par 2");
		clues.add("Je suis la racine carr� de 4");
		clues.add("Je suis un chiffre premier");
	}

	@After
	public void tearDown() throws Exception {
		q=null;
		clues=null;
	}

	@Test
	public void testGetClue() {
		assertEquals("Test de getClue(0)","Je suis divisible par 2", q.getClue(0));
		assertEquals("Test de getClue(1)","Je suis la racine carr� de 4", q.getClue(1));
		assertEquals("Test de getClue(2)","Je suis un chiffre premier", q.getClue(2));
	}
	
	@Test
	public void testEquals(){
		assertTrue("Test 1 de equals()",q.equals(new Question("Jean-Pierre","Calcul","2")));
		assertTrue("Test 2 de equals()",!q.equals(new Question("Jean-Pierre","Calcul","3")));
		assertTrue("Test 3 de equals()",!q.equals(new Question("Jean-Pierre","Chiffre","2")));
		assertTrue("Test 4 de equals()",!q.equals(new Object()));
	}
	
	@Test
	public void testAddClue(){
		clues.clear();
		q.addClue("clue 1");
		assertEquals("test de addClue()",clues.get(0),"clue 1");
	}
	
	@Test
	public void testClone(){
		Question q2 = q.clone();
		assertTrue("Test de clone()",q.equals(q2));
	}

	@Test
	public void testTostring(){
		String resultat = "author : Alexandre\ntheme : Calcul\nanswer : 2\nclue 1 : Je suis divisible par 2\nclue 2 : Je suis la racine carr� de 4\nclue 3 : Je suis un chiffre premier\n";
		assertEquals("Test de toString()",q.toString(),resultat);
	}
}
