package tests;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import utilities.Recognition;

public class TestRecognition {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertTrue(!Recognition.recognize("Jean Pierre", "Jean Paul"));
		assertTrue(Recognition.recognize("Jean Pierre", "Jean-Pierre"));
		assertTrue(Recognition.recognize("�l�ves", "eleve"));
		assertTrue(Recognition.recognize("Les rois du monde","l� roi du mond"));
		assertTrue(Recognition.recognize("Quand elle avait eu 5 ans,elle etait incapble de prononcer une phrase complete","Quand elle avait cinq ans, elle �tait incapable de prononcer une phrase compl�te."));
		assertTrue(Recognition.recognize("Clarck Kent", "Superman"));
	}

}