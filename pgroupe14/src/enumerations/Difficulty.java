package enumerations;

public enum Difficulty {
	EASY("EASY", 0.5, 3, 120), MEDIUM("MEDIUM",0.6, 4, 120), HARD("HARD",0.7, 5, 120), EXPERT("EXPERT",0.8, 6, 120), ALL("ALL",0,0,0);
	private double value;
	private String name;
	private int delay, time;
	/**
	 * 
	 * @param value 
	 */
	Difficulty(String name, double value, int delay, int time){
		this.value = value;
		this.name = name;
		this.delay = delay;
		this.time = time;
	}
	/**
	 * 
	 * @return the value of the difficulty
	 */
	public double getValue() {
		return value;
	}

	/**
	 * 
	 * @param value 
	 * 				
	 */
	public void setValue(double value) {
		this.value = value;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public int getDelay() {
		return delay;
	}
	
	public void setDelay(int delay) {
		this.delay = delay;
	}
	
	public int getTime() {
		return time;
	}
	
	public void setTime(int time) {
		this.time = time;
	}
}
