package models;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class GestionPersonne extends AbstractObservable{

	private static GestionPersonne instance;
	private HashMap<String, User> users;
	
	/**
	 * It allows to create a HashMap of questions.
	 */
	private GestionPersonne(){
		super();
		users = new HashMap<>();
	}
	/**
	 * @return
	 *The GestionPersonne instance which is a copy of the true deck
	 */
	public static GestionPersonne getInstance(){
		if(instance == null){
			instance = new GestionPersonne();
		}
		return instance;
	}
	/**
	 * 
	 * @return
	 * It returns the size of the list.
	 */
	public int size(){
		return toArrayList().size();
	}
	/**
	 * It converts a hashmap values into ArrayList.
	 * @return
	 * It returns a List of users
	 */
	public ArrayList<User> toArrayList(){
		return new ArrayList<User>(users.values());
	}
	/**
	 * 
	 * @param index It's the localisation of the user.
	 * @return
	 * return a user
	 */
	public User getUser (int index){
		return toArrayList().get(index);
	}
	/**
	 * 
	 * @param u
	 * A new user will be added	
	 */
	public void addUser(User u){
		if(!users.containsKey(u.getNickname())){
			users.put(u.getNickname(), u);
			notifie();
		}
	}
	/**
	 * When you write the nickname, it gives the user.
	 * @param nickname
	 * It's the nickname of the user.
	 * @return
	 * return a user
	 */
	public User getUser(String nickname){
		return users.get(nickname);
	}
	/**
	 * 
	 * @return
	 * return a hashmap of users
	 */
	public HashMap<String, User> getUsers(){
		HashMap<String, User> clone = new HashMap<>();
		users.forEach((k,v)->clone.put(k, v.clone()));
		return clone;
	}
	/**
	 * @return
	 * it returns a String
	 */
	public String toString(){
		String tmp = "";
		for(Entry<String, User> entry : users.entrySet()) {
		    tmp+=entry.getValue().toString()+"\n";
		}
		return tmp.substring(0,tmp.length()-1);
	}
	/**
	 *
	 * It allows to initialize the GestionPersonne from a file (Users.json)
	 */
	public void fromJson(String file){
		Gson json = new Gson();
		Type listType = new TypeToken<List<User>>() {}.getType();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<User> data = json.fromJson(br, listType);
		for(User u : data){
			GestionPersonne.getInstance().addUser(u);
		}
	}
	/**
	 * It allows to write data into a file
	 * @param file
	 * It's the file where the data is written
	 */
	public void toJson(String file){
		try(Writer writer = new OutputStreamWriter(new FileOutputStream(file) , "UTF-8")){
			Gson gson = new GsonBuilder().create();
			gson.toJson(GestionPersonne.getInstance().toArrayList(), writer);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * it removes a user
	 * @param i
	 *	It's the location of the user.
	 */
	public void remove(int i) {
		users.remove(getUser(i).getNickname());
		notifie();
	}
	
}
