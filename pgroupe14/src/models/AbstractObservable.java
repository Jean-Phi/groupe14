package models;

import java.util.ArrayList;
import java.util.List;


public abstract class AbstractObservable {

	public List<Observeur> observeurs;


	public AbstractObservable() {
		observeurs = new ArrayList<Observeur>();
	}

	public void ajoute(Observeur o) {
		observeurs.add(o);	
	}

	public void retire(Observeur o) {
		observeurs.remove(o);
	}

	public void notifie() {
		for (Observeur o : observeurs) {
			o.actualise();
		}	
	}
}
