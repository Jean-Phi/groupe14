package models;

public class User {
	private String nickname;
	private Integer score;
	private String password;
	private Boolean admin;
	
	/**
	 * It allows to create a user.
	 * @param nickname
	 * User's name
	 */
	public User(String nickname,String password){
		this(nickname, password, false);
	}
	/**
	 * It allows to create a user
	 * @param nickname
	 * user's name
	 * @param password
	 * user's password
	 * @param admin
	 * it allows to be an admin
	 */
	public User(String nickname, String password, boolean admin){
		this.nickname=nickname;
		this.password=password;
		this.admin=admin;
		this.score=0;
	}
	/**
	 * 
	 * @return
	 * return the nickname's user
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * Update the user's name
	 * @param nickname
	 * The new user's name
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * 
	 * @return
	 * return the socore of player
	 */
	public int getScore() {
		return score;
	}
	/**
	 * Update the user's score
	 * @param score
	 * new user's score
	 */
	public void setScore(int score) {
		this.score = score;
	}
	/**
	 * 
	 * @param o
	 * Variable object wich allows to check if there is no duplication
	 * @return
	 * return true if the user is already present and false if he is not.
	 */
	public boolean Equals(Object o){
		if(o instanceof User){
			return ((User)o).getNickname().equals(getNickname());
		}
		return false;
	}
	/**
	 * increase the user's score
	 */
	public void addPoint(){
		this.score++;
	}
	/**
	 * put the user's score to 0
	 */
	public void resetScore(){
		this.score=0;
	}
	
	/**
	 * @return
	 * The user's password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * Modify the password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * modify the score
	 * @param score
	 */
	public void setScore(Integer score) {
		this.score = score;
	}
	/**
	 * @return
	 * return the user's state
	 */
	public Boolean isAdmin() {
		return admin;
	}
	/**
	 * change the user's state
	 * @param admin
	 */
	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}
	/**
	 * it clones the user
	 */
	protected User clone(){
		return new User(nickname, password, admin);
	}
	/**
	 * it returns the user's string
	 */
	@Override
	public String toString() {
		return "User [nickname=" + nickname + ", score=" + score + ", password=" + password + ", admin=" + admin + "]";
	}
}
