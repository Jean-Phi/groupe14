package models;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;import java.awt.FontMetrics;
import java.awt.Graphics2D;
import javax.imageio.ImageIO;
import javax.swing.JLabel;


public class TexturedJLabel extends JLabel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image background ;

	/**
	 * It allows to create a TexturedJLabel
	 * @param text
	 * It's the text inside the JLabel
	 * @param fileName
	 * It's the name of the file
	 */
	public TexturedJLabel(String text, String fileName) {
		super(text);
		try {
			background = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Change the background of an image
	 * @param fileName
	 */
	public void setImageBackground(String fileName){
		try {
			background = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * put a background into the JLabel
	 */
	@Override
	public void paintComponent(Graphics g) {

		FontMetrics fm = g.getFontMetrics();
		int totalWidth = (fm.stringWidth(getText()) / 2);
		int x = (getWidth()/2-totalWidth),
				y = ((getHeight() - fm.getHeight()) / 2) + fm.getAscent();
		g.drawString(getText(), x, y);
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), this);
		g2d.drawString(this.getText(), x, y);
		super.paintComponents(g);
	}


}


