package models;

public interface Observeur {

	public void actualise();
}
