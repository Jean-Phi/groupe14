package models;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JProgressBar;
import javax.swing.Timer;

import gui.GamePanel;

public class ChronoBar extends JProgressBar {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private boolean isRunning;
	/** timer : timer servant � d�cr�menter le chronometre */
	private Timer timer;

	/** tempsRestant : temps restant */
	private int tempsRestant;  

	/** temps : temps initial */
	private int temps;
	
	/**
	 * construct the ChronoBar
	 * @param time 
	 * It s the time displayed on the bar 
	 */
	public ChronoBar(int time) {
		super(JProgressBar.VERTICAL);
		timer = createTimer ();
		this.setMinimum(0);
		this.setMaximum(time);
		this.setTempsRestant(time);
		this.setForeground(Color.blue);
		this.setTemps(time);
	}
	/**
	 * modify the remaining time
	 * @param tempsRestant
	 */
	public void setTempsRestant(int tempsRestant) {
		this.tempsRestant = tempsRestant; 
	}
	/**
	 * modify the time
	 * @param temps
	 */
	public void setTemps(int temps) {
		this.temps = temps;
	}

	public void setTimer(Timer timer) {
		this.timer = timer;
	}

	public Timer getTimer() {
		return timer;
	}

	public int getTempsRestant() {
		return tempsRestant;
	}

	public int getTemps() {
		return temps;
	}

	public boolean isRunning() {
		return isRunning;
	}

	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}

	public void start(){
		timer.start ();
		setRunning(true);
	}

	public void stop(){
		timer.stop ();
		setRunning(false);
		((GamePanel)getParent()).stop();
	}

	private Timer createTimer (){
		ActionListener action = new ActionListener (){

			public void actionPerformed (ActionEvent event){
				if(tempsRestant>0){
					setValue(tempsRestant--);
					repaint();
				}else{
					stop();
				}
			}
		};
		return new Timer (1000, action);
	}

	public void reset(int time) {
		this.setMinimum(0);
		this.setMaximum(time);
		this.setTempsRestant(time);
		this.setTemps(time);
	}

}