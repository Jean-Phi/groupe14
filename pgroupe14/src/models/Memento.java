package models;

public class Memento<T> {

	private T state;
	/**
	 * 
	 * @param state
	 * The state to save
	 */
	public Memento(T state){
		this.state=state;
	}
	/**
	 * 
	 * @return the state saved
	 */
	public T getState(){
		return state;
	}
}
