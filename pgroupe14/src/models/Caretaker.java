package models;

import java.util.ArrayList;

public class Caretaker<T> {

	private ArrayList<Memento<T>> mementos;

	public Caretaker(){
		mementos = new ArrayList<>();
	}
	/**
	 * Hold a memento in memory
	 * @param memento to save
	 */
	public void addMemento(Memento<T> memento) {
		mementos.add(memento);
	}
	/**
	 * Returns the memento at the specified position in this list.
	 */
	public Memento<T> getMemento(int index) {
		return mementos.get(index);
	}
	/**
	 * Returns the first memento hold in memory
	 * @return the first memento hold in memory
	 */
	public Memento<T> returnToZero(){
		return mementos.get(0);
	}

	public Memento<T> undo(){
		if(mementos.size()>1){
			mementos.remove(mementos.size()-1);
			return mementos.get(mementos.size()-1);
		}
		return mementos.get(0);
	}

	public int size() {
		return mementos.size();
	}

}
