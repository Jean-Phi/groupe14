package models;

import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class Audio extends Thread
{
	private URL url;
	private AudioClip sound;
	private boolean isPlaying;
	/**
	 * construct the Audio
	 * @param url1 is the way to find the file.
	 */
	public Audio(String url1)
	{
		File file=new File(url1);
		try {
			url=file.toURI().toURL();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sound = Applet.newAudioClip(url);
	}
	/**
	 * it makes the sound play
	 */
	public void jouer()
	{
		sound.play();
		isPlaying=true;
	}
	/**
	 * with this function the sound is never stopped
	 */
	public void jouerEnBoucle()
	{
		sound.loop();
		isPlaying=true;
	}
	/**
	 * it stops the sound
	 */
	public void arreter()
	{
		sound.stop();
		isPlaying=false;
	}
	/**
	 * 
	 * @return the url of the file sound
	 */
	public URL getUrl() {
		return url;
	}
	/**
	 * modify the url of the file
	 * @param url
	 */
	public void setUrl(URL url) {
		this.url = url;
	}
	/**
	 * 
	 * @return the sound
	 */
	public AudioClip getSound() {
		return sound;
	}
	/**
	 * modify the sound
	 * @param sound
	 */
	public void setSound(AudioClip sound) {
		this.sound = sound;
	}
	/**
	 * it allows to know if the sound is playing 
	 * @return
	 */
	public boolean isPlaying() {
		return isPlaying;
	}
	/**
	 * it allows to modify the state of the sound
	 * @param isPlaying
	 */
	public void setPlaying(boolean isPlaying) {
		this.isPlaying = isPlaying;
	}
}