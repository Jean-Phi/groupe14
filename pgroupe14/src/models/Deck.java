package models;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class Deck extends AbstractObservable{
	private static Deck instance;
	private List<Question> questions;
	private static Caretaker<List<Question>> caretaker;

	/**
	 * It allows to create a list of questions.
	 */
	private Deck (){
		super();
		questions = new ArrayList<Question>();
	}
	/**
	 * @return
	 *The deck instance which is a copy of the true deck
	 */
	public static Deck getInstance(){
		if(instance == null){
			instance=new Deck();
			caretaker = new Caretaker<>();
			caretaker.addMemento(instance.save());
		}
		return instance;
	}
	
	/*
	 * Methode Memento 
	 */
	private static void restore (Memento<List<Question>> memento) {
		setState(memento.getState());
	}
	
	/*
	 * Methode Memento 
	 */
	private static void setState(List<Question> state) {
		instance.questions = state;
		instance.notifie();
	}
	
	/*
	 * Methode Memento 
	 */
	private Memento<List<Question>> save() {
		List<Question> list = new ArrayList<>();
		list.addAll(questions);
		return new Memento<>(list);
	}
	/*
	 * Methode Memento
	 */
	public static void undo(){
		restore(caretaker.undo());
	}
	private List<Question> getList(){
		return questions;
	}
	
	/**
	 * 
	 * @param q
	 * A new question will be added		
	 */
	public void addQuestion(Question q){
		if(!questions.contains(q)){
			caretaker.addMemento(instance.save());//Save to careTaker
			questions.add(q);
			notifie();
		}
	}

	/**
	 * 
	 * @param index
	 * The index of the question in the deck
	 * @return
	 * Return the copy of the question
	 */
	public Question getQuestion(int index){
		return questions.get(index).clone();
	}
	/**
	 * It removes the question at the index specified in the arguments.
	 * @param index
	 */
	public void removeQuestion(int index){
		caretaker.addMemento(instance.save());
		questions.remove(index);
		notifie();
	}
	/**
	 * 
	 * @param theme
	 * We enter the theme
	 * @return
	 * Return a list of questions containing questions by theme
	 */
	public List<Question> getDeckTheme (String theme){
		List<Question> d = new ArrayList<Question>();
		for(Question q : questions){
			if(q.getTheme().equals(theme)){
				d.add(q.clone());
			}
		}
		return d;
	}
	/**
	 * 
	 * @return
	 * It returns the size of the list.
	 */
	public int size(){
		return questions.size();
	}
	/**
	 * @return 
	 * A string containing the questions
	 */
	public String toString(){
		String tmp = "Deck : \n";
		for(int i=0; i<size();i++){
			tmp+=questions.get(i).toString()+"\n";
		}
		return tmp;
	}
	/**
	 * 
	 * @return
	 * return the list of theme
	 */
	public List<String> getThemes(){
		List<String> themes = new ArrayList<String>();
		for(int i = 0; i<instance.size(); i++){
			if(!themes.contains(instance.getQuestion(i).getTheme())){
				themes.add(instance.getQuestion(i).getTheme());
			}
		}
		return themes;
	}

	/**
	 *
	 * It allows to initialize the deck from a file (deck.json)
	 */
	public void fromJson(String file){
		Gson json = new Gson();
		Type listType = new TypeToken<List<Question>>() {}.getType();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Question> data = json.fromJson(br, listType);
		for(Question q : data){
			Deck.getInstance().addQuestion(q);
		}
	}
	/**
	 * It allows to write data into a file
	 * @param file
	 * It's the file where the data is written
	 */
	public void toJson(String file){
		try(Writer writer = new OutputStreamWriter(new FileOutputStream(file) , "UTF-8")){
			Gson gson = new GsonBuilder().create();
			gson.toJson(Deck.getInstance().getList(), writer);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 
	 * It removes the question when you give the question in the arguments
	 * @param valueAt
	 */

	public void removeQuestion(Question valueAt) {
		caretaker.addMemento(instance.save());
		questions.remove(valueAt);
		notifie();
	}
	/**
	 * It sets the question 
	 * @param index
	 * It's the place where the question is located
	 * @param q
	 * It's the question 
	 */
	public void setQuestion(int index, Question q){
		caretaker.addMemento(instance.save());
		questions.set(index, q);
		notifie();
	}
	/**
	 * It gives the location of the question
	 * @param q
	 * It's the question
	 * @return
	 * The place where the question is.
	 */
	public int indeoxOf(Question q){
		return questions.indexOf(q);
	}
}
