package models;


import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsEnvironment;

import javax.imageio.ImageIO;
import javax.swing.JButton;

public class TexturedJButton extends JButton {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Image background ;
	/**
	 * It allows to create a TexturedJButton
	 * @param text
	 * It's the text inside the button
	 * @param fileName
	 * It's the name of the file
	 */
	
	public TexturedJButton(String text, String fileName) {
		super(text);
		changeFont();
		try {
			background = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Change the background of an image
	 * @param fileName
	 */
	public void setImageBackground(String fileName){
		try {
			background = ImageIO.read(new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * put a background on the Jbutton
	 */
	@Override
	public void paintComponent(Graphics g) {

		FontMetrics fm = g.getFontMetrics();
		int totalWidth = (fm.stringWidth(getText()) / 2);
		int x = (getWidth()/2-totalWidth),
			y = ((getHeight() - fm.getHeight()) / 2) + fm.getAscent();
		g.drawString(getText(), x, y);
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), this);
		g2d.drawString(this.getText(), x, y);
		super.paintComponents(g);
	}
	/**
	 * change the police of a String
	 */
	private void changeFont(){
		Font customFont=null;
		try {
			customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Sarpanch-SemiBold.otf")).deriveFont(16f);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		//register the font
		try {
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("Sarpanch-SemiBold.otf")));
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setFont(customFont.deriveFont(customFont.getStyle()| Font.BOLD));
		
	}

}
