package models;

import enumerations.Difficulty;

public class Score implements Comparable<Score> {
	private String nickname,theme;
	private int score;
	Difficulty diff;
	/**
	 * It allows to create a score
	 * @param nickname
	 * user's name
	 * @param theme
	 * the theme of the score
	 * @param score
	 * the user's point
	 * @param difficulty
	 * the difficulty of the game
	 */
	
	public Score(String nickname, String theme, int score,Difficulty difficulty) {
		this.nickname = nickname;
		this.theme = theme;
		setScore(score);
		this.diff=difficulty;
	}
	/**
	 * 
	 * @return
	 * it returns the nickname's user
	 */
	public String getNickname() {
		return nickname;
	}
	/**
	 * it changes the nickname's user
	 * @param nickname
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * 
	 * @return
	 * it returns the theme
	 */
	public String getTheme() {
		return theme;
	}
	/**
	 * 
	 * @param theme
	 * it changes the theme
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}
	/**
	 * 
	 * @return
	 * it returns the score
	 */
	public int getScore() {
		return score;
	}
	/**
	 * it changes the score
	 * @param score
	 */
	public void setScore(int score) {
		if(score>=0){
			this.score = score;
		}
	}
	/**
	 * it returns the difficulty
	 * @return
	 */
	public Difficulty getDiff() {
		return diff;
	}
	/**
	 * it changes the difficulty
	 * @param diff
	 */
	public void setDiff(Difficulty diff) {
		this.diff = diff;
	}
	/**
	 * it's used to compare the score
	 */
	@Override
	public int compareTo(Score o) {
		Integer i = new Integer(this.score); 
		return i.compareTo(o.getScore());
	}
	


}
