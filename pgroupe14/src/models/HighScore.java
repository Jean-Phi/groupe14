package models;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import enumerations.Difficulty;

public class HighScore extends AbstractObservable{
	private List<Score> scores;
	private static HighScore instance;
	
	public static HighScore getInstance(){
		if(instance == null){
			instance = new HighScore();
		}
		return instance;
	}
	/**
	 * It allows to create a list of scores.
	 */
	public HighScore(){
		scores=new ArrayList<>();
	}
	/**
	 * @return
	 * it returns a list of scores
	 */
	private List<Score> getList(){
		return scores;
	}
	/**
	 * A new score will be added	
	 * @param s
	 * 	
	 */
	public void addScore(Score s){
		scores.add(s);
		tri();
		toJson("scores.json");
		notifie();
	}
	/**
	 * It removes the score
	 * @param s
	 */
	public void removeScore(Score s){
		scores.remove(s);
		tri();
		toJson("scores.json");
		notifie();
	}
	/**
	 * @param d
	 * it's the difficulty
	 * @return
	 * It returns a copy of list of score
	 */
	public List<Score> getScoresDifficulty(Difficulty d){
		List<Score> copy=new ArrayList<>();
		for(Score s : scores){
			if(s.getDiff().equals(d)){
				copy.add(s);
			}
		}
		return copy;
	}
	/**
	 * @return
	 * It returns the size of the list.
	 */
	public int size(){
		return scores.size();
	}
	/**
	 * 
	 * @param index
	 * It's the location of score
	 * @return
	 * it returns the score
	 */
	public Score getScore(int index){
		return scores.get(index);
	}
	/**
	 * Used to order the liste
	 */
	public void tri(){
		Collections.sort(scores);
		Collections.reverse(scores);
	}
	/**
	 *
	 * It allows to initialize the HighScore from a file (scores.json)
	 */
	public void fromJson(String file){
		Gson json = new Gson();
		Type listType = new TypeToken<List<Score>>() {}.getType();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Score> data = json.fromJson(br, listType);
		for(Score s : data){
			HighScore.getInstance().addScore(s);
		}
	}
	/**
	 * It allows to write data into a file
	 * @param file
	 * It's the file where the data is written
	 */
	public static void toJson(String file){
		try(Writer writer = new OutputStreamWriter(new FileOutputStream(file) , "UTF-8")){
			Gson gson = new GsonBuilder().create();
			gson.toJson(HighScore.getInstance().getList(), writer);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
