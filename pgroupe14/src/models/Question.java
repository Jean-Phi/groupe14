package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Question implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String author, theme, answer;
	private List<String> clues;
	/**
	 * Constructor of the class question
	 * @param author
	 * The author's name
	 * @param theme
	 * The theme of the question
	 * @param answer
	 * The answer of the question
	 */
	
	public Question(String author, String theme, String answer) {
		this.author = author;
		this.theme = theme;
		this.answer = answer;
		this.clues = new ArrayList<String>(3);
	}

	/**
	 * @return a clone of question
	 */
	public Question clone() {
		Question q = new Question(getAuthor(),getTheme(),getAnswer());
		q.addClue(getClue(0));
		q.addClue(getClue(1));
		q.addClue(getClue(2));
		return q;
	}

	/**
	 * 
	 * @param o
	 * Variable object wich allows to check if there is no duplication 
	 * @return
	 * return true if the question in the argument is the same that the question called on equals.
	 */
	public boolean equals (Object o){
		if(o instanceof Question){
			return (((Question)o).getAnswer().equals(getAnswer())&&((Question)o).getTheme().equals(getTheme()));
		}
		return false;
	}
	/**
	 * It allows to add a new clue
	 * @param clue
	 * nouvelle indice
	 */
	public void addClue(String clue){
		if(!clues.contains(clue)){
			clues.add(clue);
		}
	}
	/**
	 * 
	 * @return
	 * It returns the author's name
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * update the author's name
	 * @param author
	 * new author's name
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	/**
	 * 
	 * @return
	 * the name of the theme
	 */
	public String getTheme() {
		return theme;
	}
	/**
	 * update the theme
	 * @param theme
	 * new theme of question
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}
	/**
	 * 
	 * @return
	 * return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 * update the answer
	 * @param answer
	 * new answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	/**
	 * 
	 * @param clues
	 * It allows to modify the list of clues
	 */
	public void setClues(List<String> clues) {
		this.clues = clues;
	}
	/**
	 * 
	 * @param index
	 * index of the list of clues
	 * @return
	 * return the clue
	 */
	public String getClue(int index){
		return clues.get(index);
	}
	/**
	 * @return
	 * return a string with the author's name,theme,answer,clues
	 */
	public String toString(){
		return "author : "+getAuthor()+"\n"
				+"theme : "+getTheme()+"\n"
				+"answer : "+getAnswer()+"\n"
				+clues;
				/*+"clue 1 : "+getClue(0)+"\n"
				+"clue 2 : "+getClue(1)+"\n"
				+"clue 3 : "+getClue(2)+"\n";*/
	}

}
