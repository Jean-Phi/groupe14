package utilities;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import enumerations.Difficulty;


public class Recognition {


	private static Difficulty difficulty = Difficulty.MEDIUM;
	/**
	 * 
	 * @param expected
	 * word expected
	 * @param actual
	 * 
	 * @return true if words are similar
	 */
	public static boolean recognize (String expected, String actual){
		expected=Synonym.replaceSynonym(removeAccent(expected.toLowerCase()));
		actual=Synonym.replaceSynonym(removeAccent(actual.toLowerCase()));
		if(actual.equals(expected)){
			return true;
		}

		List<String> wordsExpected = split(expected),
				wordsActual= split(actual);
		int numberWordsExpected = wordsExpected.size();
		double count=0;
		for(String wordExpecte : wordsExpected){
			for(String wordActual : wordsActual){
				if(ComputeCorrelation(wordActual, wordExpecte)>=difficulty.getValue()){
					count++;
				}
			}
		}
		return (count/numberWordsExpected)>=difficulty.getValue();
	}

	/**
	 * @param source has to split
	 * @return List<String> (words of the source)
	 * 
	 **/
	public static List<String> split(String source){
		return new ArrayList<String>(Arrays.asList(Pattern.compile("\\W").split(source)));
	}

	public static int countWords(String source){
		return split(source).size();
	}
	/**
	 * @param source
	 * @return source without special characters
	 */	
	public static String removeAccent(String source) {
		return Normalizer.normalize(source, Normalizer.Form.NFD).replaceAll("[\u0300-\u036F]", "");
	}

	/**
	 * Computes the Levenshtein distance between 2 strings
	 * @param actual
	 * String actual
	 * @param expected
	 * String expected
	 * @return
	 * the Levenshtein distance
	 */
	public static int ComputeDistance(String actual, String expected){
		if (actual == null) {
			throw new NullPointerException("s1 must not be null");
		}

		if (expected == null) {
			throw new NullPointerException("s2 must not be null");
		}

		if (actual.equals(expected)) {
			return 0;
		}

		if (actual.length() == 0) {
			return expected.length();
		}

		if (expected.length() == 0) {
			return actual.length();
		}

		// create two work vectors of integer distances
		int[] v0 = new int[expected.length() + 1];
		int[] v1 = new int[expected.length() + 1];
		int[] vtemp;

		// initialize v0 (the previous row of distances)
		// this row is A[0][i]: edit distance for an empty s
		// the distance is just the number of characters to delete from t
		for (int i = 0; i < v0.length; i++) {
			v0[i] = i;
		}

		for (int i = 0; i < actual.length(); i++) {
			// calculate v1 (current row distances) from the previous row v0
			// first element of v1 is A[i+1][0]
			//   edit distance is delete (i+1) chars from s to match empty t
			v1[0] = i + 1;

			// use formula to fill in the rest of the row
			for (int j = 0; j < expected.length(); j++) {
				int cost = 1;
				if (actual.charAt(i) == expected.charAt(j)) {
					cost = 0;
				}
				v1[j + 1] = Math.min(
						v1[j] + 1,              // Cost of insertion
						Math.min(
								v0[j + 1] + 1,  // Cost of remove
								v0[j] + cost)); // Cost of substitution
			}

			// copy v1 (current row) to v0 (previous row) for next iteration
			//System.arraycopy(v1, 0, v0, 0, v0.length);

			// Flip references to current and previous row
			vtemp = v0;
			v0 = v1;
			v1 = vtemp;

		}

		return v0[expected.length()];
	}

	/**
	 * Computes the correlation coefficient between 2 strings, based on the Levenshtein distance
	 * @param expected
	 * String expected
	 * @param actual
	 * String actual
	 * @return
	 * The correlation coefficient between the 2 strings. (between 0 and 1)
	 */
	public static double ComputeCorrelation(String expected, String actual){
		int distance = ComputeDistance(expected, actual);
		int longest = Math.max(expected.length(), actual.length());
		return 1 - (double)distance / longest;
	}

	public static Difficulty getDifficult() {
		return difficulty;
	}
	/**
	 * update the difficulty
	 * @param difficulty
	 * 
	 */
	public static void setDifficult(Difficulty difficulty) {
		Recognition.difficulty = difficulty;
	}
}
