package utilities;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.google.gson.Gson;

import models.Deck;
import models.Question;

public class Serialisation {

	
	/**
	 * permet d'�crire un deck dans un fichier json
	 * @param 
	 * d le deck � �crire
	 * @param 
	 * file nom du fichier
	 */
	public static void writeDeck(Deck d, String file){
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file))){
			out.writeObject(d);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	/**
	 * permet d'�crire une question dans un fichier json
	 * @param 
	 * q la question � �crire
	 * @param 
	 * file nom du fichier
	 */
	public static void writeQuestion(Question q, String file){
		try(ObjectOutputStream out = new ObjectOutputStream( new FileOutputStream(file))){
			out.writeObject(q);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * permet de lire une question dans un fichier json
	 * @param 
	 * fileName nom du fichier
	 * @return
	 * instance de Question
	 */	
	public static Question readQuestion (String fileName){
		String line, file="";
		Gson json = new Gson();
		try(BufferedReader in = new BufferedReader(new FileReader(fileName))){
			line  = in.readLine();
			while (line != null){
				file+=line;
				line  = in.readLine();
			}
			return json.fromJson(file, Question.class);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return null;
	}
	/**
	 * permet de lire un deck dans un fichier json
	 * @param 
	 * fileName nom du fichier
	 * @return
	 * instance de deck
	 */
	public static Deck readDeck(String fileName){
		String line, file="";
		Gson json = new Gson();
		try(BufferedReader in = new BufferedReader(new FileReader(fileName))){
			line  = in.readLine();
			while (line != null){
				file+=line;
				line  = in.readLine();
			}
			return json.fromJson(file, Deck.class);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return null;
	}
}
