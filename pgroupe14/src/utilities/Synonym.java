package utilities;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import models.AbstractObservable;
import models.Caretaker;
import models.Memento;


public class Synonym extends AbstractObservable{

	private List<List<String>> dictionary;
	private static Synonym instance;
	private static Caretaker <List<List<String>>> caretaker;

	private Synonym(){
		super();
		dictionary = new ArrayList<>();
	}

	public static List<String> copyList(List<String> source){
		List<String> copy = new ArrayList<>(source.size());
		for(String word : source){
			copy.add(word);
		}
		return copy;
	}

	public static Synonym getInstance(){
		if(instance == null){
			instance = new Synonym();
			caretaker = new Caretaker<>();
			caretaker.addMemento(instance.save());
		}
		return instance;
	}

	/**Methode Memento
	 * restore the state from the memento
	 * @param memento who contains the state to restore
	 */
	private static void restore (Memento<List<List<String>>> memento) {
		setState(memento.getState());
	}

	/**Methode Memento
	 * set the state 
	 * @param state
	 */
	private static void setState(List<List<String>> state) {
		instance.dictionary = state;
		instance.notifie();
	}

	/**Methode Memento
	 * save the state into a memento
	 * @return a memento who contains the saved state
	 */
	private Memento<List<List<String>>> save() {
		List<List<String>> list = new ArrayList<>();
		list.addAll(dictionary);
		return new Memento<List<List<String>>>(list);
	}
	/**
	 * @param source has to correct
	 * @return new source 
	 */
	public static void undo(){
		restore(caretaker.undo());
	}

	public void addSynonym (String...arg){
		caretaker.addMemento(instance.save());
		dictionary.add(Arrays.asList(arg));
		notifie();
	}

	public String toString(){
		String tmp ="Synonym :\n";
		for(List<String> liste : dictionary){
			for(String word : liste){
				tmp+=word+", ";
			}
			tmp+="\n";
		}
		return tmp;
	}

	public List<String> getSynonyms(String word){
		for(List<String> liste : dictionary){
			if(liste.contains(word)){
				return Synonym.copyList(liste);
			}
		}
		return null;
	}

	public List<List<String>> getDitionary(){
		List<List<String>> copy = new ArrayList<>();
		for(List<String> liste : dictionary){
			copy.add(copyList(liste));
		}
		return copy;
	}

	public static void fromJson1(String fileName){
		getInstance();
		String line, file="";
		Gson json = new Gson();
		try(BufferedReader in = new BufferedReader(new FileReader(fileName))){
			line  = in.readLine();
			while (line != null){
				file+=line;
				line  = in.readLine();
			}
			addAll(json.fromJson(file, Synonym.class));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	private static void addAll(Synonym syn){
		List<List<String>> list = syn.getDitionary();
		for(List<String> item : list){
			getInstance().addSynonym(item);
		}
	}
	public static String replaceSynonym(String source){
		List<List<String>> dictionary = getInstance().getDitionary();
		List<String> sourceWords = Recognition.split(source);
		for(List<String> liste : dictionary){
			for(int i = 0; i <liste.size();i++){
				String word = liste.get(i);
				int countWord = Recognition.countWords(word);
				//System.out.println("Le mot : "+word+" est compos� de "+countWord);
				for(int j = 0; j < sourceWords.size(); j++){
					//System.out.println("Mot trait� : "+ sourceWords.get(j));
					if(j+countWord<=sourceWords.size()){
						String tmp="";
						for(int k = 0; k < countWord ; k++){
							tmp+=sourceWords.get(j+k)+" ";
						}
						tmp = tmp.substring(0, tmp.length()-1);
						//System.out.println("Tmp : "+tmp);
						if(Recognition.ComputeCorrelation(word, tmp)>=Recognition.getDifficult().getValue()){
							int beginIndex = source.indexOf(tmp),
									endIndex = beginIndex+tmp.length();
							String oldChar = source.substring(beginIndex, endIndex),
									newChar = getInstance().getSynonyms(word).get(0);
							source = source.replace(oldChar, newChar);
							//System.out.println("Mot remplac� nouvelle source : "+source);
						}
					}
				}
			}
		}
		return source;
	}

	public int size() {
		return dictionary.size();
	}

	public List<String> get(int index){
		return dictionary.get(index);
	}

	public int indexOf(List<String> list) {
		return dictionary.indexOf(list);
	}

	public void setSynonym(int index, List<String> synonym) {
		caretaker.addMemento(instance.save());
		dictionary.set(index, synonym);
		notifie();
	}

	public void removeSynonym(int index) {
		caretaker.addMemento(instance.save());
		dictionary.remove(index);
		notifie();
	}

	public void addSynonym(List<String> l) {
		caretaker.addMemento(instance.save());
		dictionary.add(l);
		notifie();
	}
	public void toJson(String file){
		try(Writer writer = new OutputStreamWriter(new FileOutputStream(file) , "UTF-8")){
			Gson gson = new GsonBuilder().create();
			gson.toJson(Synonym.getInstance().getDitionary(), writer);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void fromJson(String file){
		Gson json = new Gson();
		Type listType = new TypeToken<List<List<String>>>() {}.getType();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<List<String>> data = json.fromJson(br, listType);
		for(List<String> s : data){
			Synonym.getInstance().addSynonym(s);
		}
	}

}