package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import models.Deck;
import models.Observeur;
import models.Question;
import models.TexturedJButton;

class DeckTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String [] nomColonnes = {"Theme", "Author", "Anwser","Clue1","Clue2","Clue3"};
	private String theme="All";
	private int rowCount = Deck.getInstance().size();

	public void setRowCount(){
		if(theme.equals("All")){
			rowCount = Deck.getInstance().size();
		}else{
			rowCount = Deck.getInstance().getDeckTheme(theme).size();
		}
	}

	public void setTheme(String theme){
		this.theme = theme;
		setRowCount();
	}

	@Override
	public int getColumnCount() {
		return nomColonnes.length;
	}

	public void addRow(Object o){
		this.addRow(o);
	}

	@Override
	public int getRowCount() {
		setRowCount();
		return rowCount;
	}

	public Object getValueAt(int ligne){
		setRowCount();
		Question q=null;
		if(theme.equals("All")){
			q = Deck.getInstance().getQuestion(ligne);
		}else{
			q = Deck.getInstance().getDeckTheme(theme).get(ligne);
		}
		return q;
	}

	@Override
	public Object getValueAt(int ligne, int colonne) {
		setRowCount();
		Question q=null;
		if(theme.equals("All")){
			q = Deck.getInstance().getQuestion(ligne);
		}else{
			q = Deck.getInstance().getDeckTheme(theme).get(ligne);
		}
		String result ="";
		switch(colonne){
		case 0: result = q.getTheme(); break;
		case 1 : result = q.getAuthor(); break;
		case 2 : result = q.getAnswer(); break;
		case 3 : result = q.getClue(0); break;
		case 4 : result = q.getClue(1); break;
		case 5 : result = q.getClue(2); break;
		}
		return result;
	}

	@Override
	public String getColumnName(int column) {
		return nomColonnes [column];
	}
	public void setValueAt(Object value, int row, int col){
		Question q=Deck.getInstance().getQuestion(row);
		List<String> liste = new ArrayList<>();
		switch(col){
		case 0:  q.setTheme((String)value); break;
		case 1 : q.setAuthor((String)value); break;
		case 2 : q.setAnswer((String)value); break;
		case 3 : liste.add((String)value); 
		liste.add(q.getClue(1)); 
		liste.add(q.getClue(2));
		q.setClues(liste);
		break;
		case 4 : liste.add((String)value); 
		liste.add(q.getClue(0)); 
		liste.add(q.getClue(2));
		q.setClues(liste);
		break;
		case 5 : liste.add((String)value); 
		liste.add(q.getClue(0)); 
		liste.add(q.getClue(1));
		q.setClues(liste);
		break;
		}
		Deck.getInstance().setQuestion(row, q);
		fireTableCellUpdated(row, col);


	}
	public boolean isCellEditable(int arg0, int arg1) {
		return true;
	}
}
/**===================================================================================================================**
 *
 * 													class DeckTablePanel
 *
 **===================================================================================================================**/
public class DeckTablePanel extends JPanel implements Observeur{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle;
	private JButton jbBack, jbDelete, jbCreation, jbUndo, jbEdit;
	private JTable jtable;
	private JScrollPane jsTable;
	private JComboBox<String> jcTheme;

	public DeckTablePanel (){
		//PopUp();
		Deck.getInstance().ajoute(this);
		GroupLayout gl=new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);

		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(700))
				.addComponent(getJlTitle())
				.addComponent(getJsTable(),765,765,765)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbBack())
						.addComponent(getJbUndo())
						.addComponent(getJbDelete())
						.addComponent(getJbEdit())
						.addComponent(getJbCreation())
						.addComponent(getJcTheme())
						)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addComponent(getJlTitle())
				.addComponent(getJsTable())
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbBack(),40,40,40)
						.addComponent(getJbUndo(),40,40,40)
						.addComponent(getJbDelete(),40,40,40)
						.addComponent(getJbEdit(),40,40,40)
						.addComponent(getJbCreation(),40,40,40)
						.addComponent(getJcTheme(),40,40,40)
						)
				);
	}

	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new JLabel("Deck");
			jlTitle.setForeground(Color.white);
		}
		return jlTitle;
	}

	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","stone.png");
			jbBack.setForeground(Color.white);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					back();
				}
				
			});
		}
		return jbBack;
	}

	public JScrollPane getJsTable() {
		if(jsTable == null){
			jsTable = new JScrollPane(getJtable());
		}
		return jsTable;
	}

	public JTable getJtable() {
		if(jtable == null){
			jtable = new JTable(new DeckTableModel());
		}
		return jtable;
	}


	public JButton getJbDelete() {
		if(jbDelete == null){
			jbDelete = new TexturedJButton("Delete","stone.png");
			jbDelete.setForeground(Color.white);
			jbDelete.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					int rows[] = getJtable().getSelectedRows();
					DeckTableModel mt = (DeckTableModel)getJtable().getModel();
					for(int i = rows.length-1; i>=0; i--){
						Deck.getInstance().removeQuestion((Question)mt.getValueAt(rows[i]));
						Deck.getInstance().	toJson("Questions.json");
					}
					mt.setRowCount();
				}
			});
		}
		return jbDelete;
	}

	public JButton getJbCreation() {
		if(jbCreation == null){
			jbCreation = new JButton("Create question");
			jbCreation = new TexturedJButton("Create question","stone.png");
			jbCreation.setForeground(Color.white);
			jbCreation.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					JButton o = (JButton) e.getSource();
					AdminCardPanel parent = (AdminCardPanel) getParent();
					CardLayout card = (CardLayout) parent.getLayout();
					card.show(parent, o.getText());
					((QuestionCreationPanel) parent.getJpQuestionCreation()).init();
				}
			});
		}
		return jbCreation;
	}

	public JButton getJbUndo() {
		if(jbUndo == null){
			jbUndo = new TexturedJButton("Undo","stone.png");
			jbUndo.setForeground(Color.white);
			jbUndo.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					Deck.undo();
				}
			});
		}
		return jbUndo;
	}

	public JButton getJbEdit() {
		if(jbEdit == null){
			jbEdit = new TexturedJButton("Edit","stone.png");
			jbEdit.setForeground(Color.white);
			jbEdit.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					edit();
					Deck.getInstance().	toJson("Questions.json");
				}
			});
		}
		return jbEdit;
	}

	public JComboBox<String> getJcTheme() {
		if(jcTheme == null){
			jcTheme = new JComboBox<>(Deck.getInstance().getThemes().toArray(new String[Deck.getInstance().getThemes().size()]));
			jcTheme.addItem("All");
			jcTheme.setSelectedItem("All");
			jcTheme.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					DeckTableModel mt = (DeckTableModel)getJtable().getModel();
					if(getJcTheme().getSelectedItem()==null){
						mt.setTheme("All");
					}else{
						mt.setTheme((String)getJcTheme().getSelectedItem());
					}
					((DeckTableModel)getJtable().getModel()).setRowCount();
					getJsTable().repaint();
				}
			});
		}
		return jcTheme;
	}

	private void back(){
		AdminCardPanel parent = (AdminCardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Admin option");
	}

	public void init(){
		getJcTheme().setSelectedItem("All");
	}

	public void edit(){
		AdminCardPanel parent = (AdminCardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent,"Create question");
		int index = getJtable().getSelectedRow();
		DeckTableModel mt = (DeckTableModel)getJtable().getModel();
		Question q = (Question)mt.getValueAt(index);
		((QuestionCreationPanel) parent.getJpQuestionCreation()).editQuestion(q);;
	}
	
	@Override
	public void actualise() {
		DeckTableModel mt = (DeckTableModel) getJtable().getModel();
		mt.fireTableDataChanged();
		mt.fireTableRowsDeleted(0, mt.getRowCount());
		getJcTheme().removeAllItems();
		List<String> items = Deck.getInstance().getThemes();
		for(String item : items){
			getJcTheme().addItem(item);
		}
		getJcTheme().addItem("All");
		getJcTheme().setSelectedItem("All");
		((DeckTableModel)getJtable().getModel()).setTheme("All");
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("scoreboardBG.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
}