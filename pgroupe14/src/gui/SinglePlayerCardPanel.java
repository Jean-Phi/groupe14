package gui;

import java.awt.CardLayout;

import javax.swing.JPanel;

public class SinglePlayerCardPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel jpThemeSelectionPanel, jpGamePanel, jpEndGameScorePanel;
	private CardLayout cardLayout;
	/**
	 * SinglePlayerCardPanel construction
	 */
	public SinglePlayerCardPanel() {
		this.setOpaque(false);
		cardLayout=new CardLayout();
		this.setLayout(cardLayout);
		this.add(getJpThemeSelectionPanel(), "ThemeSelectionPanel");
		this.add(getJpGamePanel(),"GamePanel");
		this.add(getJpEndGameScorePanel(),"EndGameScorePanel");
	}
/**
 * create a ThemeSelectionPanel
 * @return ThemeSelectionPanel
 */
	public JPanel getJpThemeSelectionPanel() {
		if(jpThemeSelectionPanel == null){
			jpThemeSelectionPanel = new ThemeSelectionPanel();
		}
		return jpThemeSelectionPanel;
	}
/**
 * create a GamePanel
 * @return GamePanel
 */
	public JPanel getJpGamePanel() {
		if(jpGamePanel == null){
			jpGamePanel = new GamePanel();
		}
		return jpGamePanel;
	}
/**
 * create an EndGameScorePanel
 * @return EndGameScorePanel
 */
	public JPanel getJpEndGameScorePanel() {
		if(jpEndGameScorePanel == null){
			jpEndGameScorePanel = new EndGameScorePanel();
		}
		return jpEndGameScorePanel;
	}

}
