package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import models.GestionPersonne;
import models.Observeur;
import models.TexturedJButton;
import models.User;

class UsersTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String [] nomColonnes = {"Nickname", "Password", "Admin"};


	@Override
	public int getColumnCount() {
		return nomColonnes.length;
	}

	public void addRow(Object o){
		this.addRow(o);
	}

	@Override
	public int getRowCount() {
		return GestionPersonne.getInstance().size();
	}

	@Override
	public Object getValueAt(int ligne, int colonne) {
		User u = GestionPersonne.getInstance().getUser(ligne);
		String result ="";
		switch(colonne){
		case 0 : result = u.getNickname(); break;
		case 1 : result = u.getPassword(); break;
		case 2 : result = (u.isAdmin()?"oui":"non"); break;
		}
		return result;
	}

	@Override
	public String getColumnName(int column) {
		return nomColonnes [column];
	}
}

public class UsersTablePanel extends JPanel implements Observeur{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle;
	private JButton jbBack, jbDelete, jbCreation;
	private JTable jtable;
	private JScrollPane jsTable;

	public UsersTablePanel(){
		GroupLayout gl=new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);
		GestionPersonne.getInstance().ajoute(this);
		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addComponent(getJlTitle())
				.addComponent(getJsTable(),750,750,750)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbBack(),200,200,200)
						.addComponent(getJbCreation(),200,200,200)
						.addComponent(getJbDelete(),200,200,200)
						)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addComponent(getJlTitle())
				.addComponent(getJsTable())
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbBack(),40,40,40)
						.addComponent(getJbCreation(),40,40,40)
						.addComponent(getJbDelete(),40,40,40)
						)
				);
	}

	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new JLabel("Users");
			jlTitle.setForeground(Color.white);
		}
		return jlTitle;
	}

	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","stone.png");
			jbBack.setForeground(Color.white);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					back();
				}
			});
		}
		return jbBack;
	}

	public JScrollPane getJsTable() {
		if(jsTable == null){
			jsTable = new JScrollPane(getJtable());
		}
		return jsTable;
	}

	public JTable getJtable() {
		if(jtable == null){
			jtable = new JTable(new UsersTableModel());
		}
		return jtable;
	}

	public JButton getJbDelete() {
		if(jbDelete == null){
			jbDelete = new JButton("Delete");
			jbDelete = new TexturedJButton("Delete","stone.png");
			jbDelete.setForeground(Color.white);
			jbDelete.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					int rows[] = getJtable().getSelectedRows();
					for(int i = rows.length-1; i>=0; i--){
						GestionPersonne.getInstance().remove(rows[i]);
					}
					GestionPersonne.getInstance().toJson("Users.json");
				}
			});
		}
		return jbDelete;
	}

	public JButton getJbCreation() {
		if(jbCreation == null){
			jbCreation = new JButton("Create user");
			jbCreation = new TexturedJButton("Create user","stone.png");
			jbCreation.setForeground(Color.WHITE);
			jbCreation.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					JButton o = (JButton) e.getSource();
					AdminCardPanel parent = (AdminCardPanel) getParent();
					CardLayout card = (CardLayout) parent.getLayout();
					card.show(parent, o.getText());
				}
			});
		}
		return jbCreation;
	}

	private void back(){
		AdminCardPanel parent = (AdminCardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Admin option");
	}

	@Override
	public void actualise() {
		UsersTableModel mt = (UsersTableModel) getJtable().getModel();
		mt.fireTableDataChanged();
		mt.fireTableRowsDeleted(0, mt.getRowCount());

	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("scoreboardBG.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
}