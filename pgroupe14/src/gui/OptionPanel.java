package gui;

import java.awt.CardLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Pattern;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import enumerations.Difficulty;
import models.HighScore;
import models.TexturedJButton;
import models.TexturedJLabel;
import utilities.Recognition;

public class OptionPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle, jlDifficulty, jlTime, jlDelay,jlSound;
	private JButton jbBack, jbSave, jbAdminOption,jbSound;
	private JTextField jtTime, jtDelay;
	private JComboBox<Difficulty> jcDifficulties;
	/**
	 * It allows to position differents elements on the panel
	 */
	public OptionPanel(){
		GroupLayout gl=new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);

		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addComponent(getJlTitle(),300,300,300)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlDifficulty(),200,200,200)
						.addGap(30)
						.addComponent(getJcDifficulties(),200,200,200)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlTime(),200,200,200)
						.addGap(30)
						.addComponent(getJtTime(),200,200,200)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlDelay(),200,200,200)
						.addGap(30)
						.addComponent(getJtDelay(),200,200,200)
						)
				.addGroup(gl.createSequentialGroup()
						.addGap(30)
						.addComponent(getJlSound(),200,200,200)
						.addComponent(getJbSound(),70,70,70)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbBack(),180,180,180)
						.addComponent(getJbAdminOption(),180, 180, 180)
						.addComponent(getJbSave(),180,180,180)

						)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(20)
				.addComponent(getJlTitle(),60,60,60)
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlDifficulty(),40,40,40)
						.addComponent(getJcDifficulties(),40,40,40)
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlTime(),40,40,40)
						.addComponent(getJtTime(),40,40,40)
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlDelay(),40,40,40)
						.addComponent(getJtDelay(),40,40,40)
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlSound(),40,40,40)
						.addComponent(getJbSound(),40,40,40)
						)
				.addGap(60)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbBack(),40,40,40)
						.addComponent(getJbAdminOption(),40,40,40)
						.addComponent(getJbSave(),40,40,40)
						.addComponent(getJbSound(),40,40,40)
						)
				);
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlTitle
	 */
	public JLabel getJlTitle() {
		if(jlTitle == null){

			jlTitle = new TexturedJLabel("Option","Parchemin.png");
			changePolice(jlTitle);
		}
		return jlTitle;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlSound
	 */
	public JLabel getJlSound() {
		if(jlSound==null){
			jlSound=new JLabel("Sound :");
			changePolice(jlSound);
		}
		return jlSound;
	}
	/**
	 * it creates a JButton
	 * @return
	 * return a JButton jbBack
	 */
	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","parcho.png");
			hideButton(jbBack);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					back();
				}
			});
		}
		return jbBack;
	}
	/**
	 * it creates a JComboBox with different levels of difficulty 
	 * @return
	 * it returns a JComboBox
	 */
	public JComboBox<Difficulty> getJcDifficulties() {
		if(jcDifficulties == null){
			Difficulty arg0[] = {Difficulty.EASY, Difficulty.MEDIUM, Difficulty.HARD, Difficulty.EXPERT};
			jcDifficulties = new JComboBox<>(arg0);
			jcDifficulties.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					showDifficulty();
				}
			});
		}
		return jcDifficulties;
	}
	/**
	 * it creates a button save wich allows to select the difficulty of your game
	 * @return
	 * return a JButton
	 */
	public JButton getJbSave() {
		if(jbSave == null){
			jbSave = new TexturedJButton("Save","parcho.png");
			hideButton(jbSave);
			jbSave.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					Difficulty d = (Difficulty)getJcDifficulties().getSelectedItem();
					if(d.getDelay()!=Integer.parseInt(getJtDelay().getText())||(d.getTime()!=Integer.parseInt(getJtTime().getText()))){
						suppScore(d);	
					}
					Recognition.setDifficult(d);
					d.setDelay(Integer.parseInt(getJtDelay().getText()));
					d.setTime(Integer.parseInt(getJtTime().getText()));
					back();
				}
			});
		}
		return jbSave;
	}
	/**
	 * it creates a JLabel JlDifficulty
	 * @return
	 * it returns a JLabel
	 */
	public JLabel getJlDifficulty() {
		if(jlDifficulty == null){
			jlDifficulty = new JLabel("Difficulty :");
			jlDifficulty.setHorizontalAlignment(SwingConstants.RIGHT);
			changePolice(jlDifficulty);
		}
		return jlDifficulty;
	}
	/**
	 * it creates a JLabel JlTime
	 * @return
	 * it returns a JLabel
	 */
	public JLabel getJlTime() {
		if(jlTime == null){
			jlTime = new JLabel("Time :");
			jlTime.setHorizontalAlignment(SwingConstants.RIGHT);
			changePolice(jlTime);

		}
		return jlTime;
	}
	/**
	 * it creates a JLabel JlDelay
	 * @return
	 * it returns a JLabel
	 */
	public JLabel getJlDelay() {
		if(jlDelay == null){
			jlDelay = new JLabel("Delay :");
			jlDelay.setHorizontalAlignment(SwingConstants.RIGHT);
			changePolice(jlDelay);
		}
		return jlDelay;
	}
	/**
	 * it creates a JTextField JtTime
	 * @return
	 * it returns a JTextField
	 */
	public JTextField getJtTime() {
		if(jtTime == null){
			jtTime = new JTextField();
			jtTime.setEnabled(false);
			jtTime.addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {
					if(Pattern.matches("\\D",""+e.getKeyChar())){
						e.consume();
					}
				}

				@Override
				public void keyPressed(KeyEvent arg0) {}

				@Override
				public void keyReleased(KeyEvent arg0) {}

			});
		}
		return jtTime;
	}
	/**
	 * it creates a JTextField wich display the time 
	 * @return
	 * return a JTextField
	 */
	public JTextField getJtDelay() {
		if(jtDelay == null){
			jtDelay = new JTextField();
			jtDelay.setEnabled(false);
			jtDelay.addKeyListener(new KeyListener() {

				@Override
				public void keyTyped(KeyEvent e) {
					if(Pattern.matches("\\D",""+e.getKeyChar())){
						e.consume();
					}
				}

				@Override
				public void keyPressed(KeyEvent arg0) {}

				@Override
				public void keyReleased(KeyEvent arg0) {}

			});
		}
		return jtDelay;
	}
	/**
	 * it creates a JButton wich allows the admin to go to his option 
	 * @return
	 * return a button
	 */
	public JButton getJbAdminOption() {
		if(jbAdminOption == null){
			jbAdminOption = new TexturedJButton("Admin Option","parcho.png");
			jbAdminOption.setVisible(false);
			hideButton(jbAdminOption);
			jbAdminOption.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					CardPanel parent = (CardPanel) getParent();
					CardLayout card = (CardLayout) parent.getLayout();
					card.show(parent,((JButton)arg0.getSource()).getText());
				}
			});
		}
		return jbAdminOption;
	}
	/**
	 * Allows the user to go to the previous panel
	 */
	private void back(){
		CardPanel parent = (CardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Main Menu");
	}
	/**
	 * it shows the difficulty in the JComboBox
	 */
	private void showDifficulty(){
		Difficulty difficulty = (Difficulty) getJcDifficulties().getSelectedItem();
		getJtDelay().setText(""+difficulty.getDelay());
		getJtDelay().repaint();
		getJtTime().setText(""+difficulty.getTime());
		getJtTime().repaint();
	}
	/**
	 * it shows the difficulty 
	 */
	private void initial(){
		Difficulty difficulty = Recognition.getDifficult();
		getJcDifficulties().setSelectedItem(difficulty);
		showDifficulty();
	}
	/**
	 * show the admin's button if you are an admin
	 */
	void showAdmin(){
		initial();
		CardPanel parent = (CardPanel) getParent();
		boolean visibility = parent.getPlayer().isAdmin();
		getJbAdminOption().setVisible(visibility);
		getJtDelay().setEnabled(visibility);
		getJtTime().setEnabled(visibility);
	}
	/**
	 * it makes a background
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("livre.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
	/**
	 * it changes the police
	 * @param jl The Jlabel you want to modify
	 * 
	 */
	public void changePolice(JLabel jl){
		Font fonte = new Font("TimesRoman ",Font.BOLD,30);
		jl.setFont(fonte);
	}
	/**
	 * it makes the button invisible
	 * @param jb The button you want to make invisible
	 * 
	 */
	private void hideButton(JButton jb){
		jb.setBorderPainted(false);
		jb.setOpaque(false);
	}
	/**
	 * it makes the button sound when you click on it, it actives or cancel the theme song
	 * @return
	 */
	public JButton getJbSound() {
		if(jbSound==null){
			jbSound=new TexturedJButton("","sound.gif");
			hideButton(jbSound);
			jbSound.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					settingSongs();
				}
			});
		}
		return jbSound;
	}
	/**
	 * it uses to actives or cancel the theme song
	 */
	private void settingSongs(){
		Window parent = (Window) getParent().getParent().getParent().getParent();
		if(parent.getAudio().isPlaying()){
			((TexturedJButton)getJbSound()).setImageBackground("mute.gif");
			parent.getAudio().arreter();
		}else{
			((TexturedJButton)getJbSound()).setImageBackground("sound.gif");
			parent.getAudio().jouerEnBoucle();
		}
	}
	private void suppScore(Difficulty d){
		int i;
		for(i=0;i<HighScore.getInstance().size();i++){
			if(HighScore.getInstance().getScore(i).getDiff().equals(d)){
				HighScore.getInstance().removeScore(HighScore.getInstance().getScore(i));
				i--;
			}
		}
	}
}