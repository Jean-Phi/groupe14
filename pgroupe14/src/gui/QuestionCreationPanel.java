package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import models.Deck;
import models.Question;
import models.TexturedJButton;

public class QuestionCreationPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle, jlTheme, jlAuthor, jlAnswer, jlClues;
	private JTextField jtTheme, jtAuthor, jtAnswer, jtClue1, jtClue2, jtClue3;
	private JButton jbBack, jbSave;
	private Question edition;
	

	/**
	 * It allows to position differents elements on the panel
	 */
	public QuestionCreationPanel(){
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		this.setBackground(Color.black);
		//GROUPE HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGap(800)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlTitle(),200,200,200)
						.addGap(170)
						.addComponent(getJlAuthor(),100,100,100)
						.addComponent(getJtAuthor(),200,200,200)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlTheme(),100,100,100)
						.addComponent(getJtTheme(),200,200,200)
						.addGap(70)
						.addComponent(getJlAnswer(),100,100,100)
						.addComponent(getJtAnswer(),200,200,200)
						)
				.addComponent(getJlClues())
				.addComponent(getJtClue1(),670,670,670)
				.addComponent(getJtClue2(),670,670,670)
				.addComponent(getJtClue3(),670,670,670)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbBack(),200,200,200)
						.addGap(60)
						.addComponent(getJbSave(),200,200,200)
						)
				);
		//GROUPE VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(75)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlTitle())
						.addComponent(getJlAuthor())
						.addComponent(getJtAuthor(),30,30,30)
						)
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlTheme())
						.addComponent(getJtTheme(),30,30,30)
						.addComponent(getJlAnswer(),30,30,30)
						.addComponent(getJtAnswer(),30,30,30)
						)
				.addGap(10)
				.addComponent(getJlClues())
				.addGap(20)
				.addComponent(getJtClue1())
				.addGap(20)
				.addComponent(getJtClue2())
				.addGap(20)
				.addComponent(getJtClue3())
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbBack(),40,40,40)
						.addComponent(getJbSave(),40,40,40)
						)
				);

		gl.linkSize(SwingConstants.VERTICAL, getJtAuthor(), getJtAnswer(),  getJtTheme(),getJtClue1(), getJtClue2(), getJtClue3());
	}

	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlTitle
	 */
	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new JLabel("Question :");
			changePolice(jlTitle);
		}
		return jlTitle;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlTheme
	 */
	public JLabel getJlTheme() {
		if(jlTheme == null){
			jlTheme = new JLabel("Theme :");
			changePolice(jlTheme);
		}
		return jlTheme;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlAutor
	 */
	public JLabel getJlAuthor() {
		if(jlAuthor == null){
			jlAuthor = new JLabel("Author :");
			changePolice(jlAuthor);
		}
		return jlAuthor;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlAnswer
	 */
	public JLabel getJlAnswer() {
		if(jlAnswer == null){
			jlAnswer = new JLabel("Answer :");
			changePolice(jlAnswer);
		}
		return jlAnswer;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlClues
	 */
	public JLabel getJlClues() {
		if(jlClues == null){
			jlClues = new JLabel("Clues :");
			jlClues.setHorizontalAlignment(SwingConstants.LEFT);
			changePolice(jlClues);
		}
		return jlClues;
	}
	/**
	 * it creates a JTextField
	 * @return
	 * return a JTextField JtTheme
	 */
	public JTextField getJtTheme() {
		if(jtTheme == null){
			jtTheme = new JTextField();
		}
		return jtTheme;
	}
	/**
	 * it creates a JTextField
	 * @return
	 * return a JTextField jtAuthor
	 */
	public JTextField getJtAuthor() {
		if(jtAuthor == null){
			jtAuthor = new JTextField();
			jtAuthor.setEnabled(false);
		}
		return jtAuthor;
	}
	/**
	 * it creates a JTextField
	 * @return
	 * return a JTextField jtAnswer
	 */
	public JTextField getJtAnswer() {
		if(jtAnswer == null){
			jtAnswer = new JTextField();
		}
		return jtAnswer;
	}
	/**
	 * it creates a JTextField
	 * @return
	 * return a JTextField jtClue1
	 */
	public JTextField getJtClue1() {
		if(jtClue1 == null){
			jtClue1 = new JTextField();
		}
		return jtClue1;
	}
	/**
	 * it creates a JTextField
	 * @return
	 * return a JTextField jtClu2
	 */
	public JTextField getJtClue2() {
		if(jtClue2 == null){
			jtClue2 = new JTextField();
		}
		return jtClue2;
	}

	public JTextField getJtClue3() {
		if(jtClue3 == null){
			jtClue3 = new JTextField();
		}
		return jtClue3;
	}
	/**
	 * it creates a JButton wich allows the user to go on the previous panel
	 * @return
	 * return a JButton
	 */
	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","parcho.png");
			hideButton(jbBack);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					AdminCardPanel parent = (AdminCardPanel) getParent();
					CardLayout card = (CardLayout) parent.getLayout();
					card.show(parent, "Admin option");
				}
			});
		}
		return jbBack;
	}
	/**
	 * it creates a JButton wich save the modifications
	 * @return
	 * return a JButton jbSave
	 */
	public JButton getJbSave() {
		if(jbSave == null){
			jbSave = new TexturedJButton("Save","parcho.png");
			hideButton(jbSave);
			jbSave.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					extractQuestion();
					Deck.getInstance().	toJson("Questions.json");
				}
			});
		}
		return jbSave;
	}
	/**
	 * it initializes the JTtextFields
	 */
	public void init(){
		edition=null;
		CardPanel parent = (CardPanel) getParent().getParent();
		getJtAuthor().setText(parent.getPlayer().getNickname());
		getJtAnswer().setText("");
		getJtTheme().setText("");
		getJtClue1().setText("");
		getJtClue2().setText("");
		getJtClue3().setText("");
	}
	/**
	 * it allows to exctract the questions created by the Admin
	 */
	private void extractQuestion(){
		boolean testAnswer = !getJtAnswer().getText().equals(""),
				testTheme = !getJtTheme().getText().equals(""),
				testClues = !getJtClue1().getText().equals("") && !getJtClue2().getText().equals("") && !getJtClue3().getText().equals("");
		if(testAnswer && testTheme && testClues){
			Question q = new Question(getJtAuthor().getText(), getJtTheme().getText(), getJtAnswer().getText());
			q.addClue(getJtClue1().getText());
			q.addClue(getJtClue2().getText());
			q.addClue(getJtClue3().getText());
			if(edition!=null){
				Deck.getInstance().setQuestion(Deck.getInstance().indeoxOf(edition), q);
			}else{
				Deck.getInstance().addQuestion(q);
			}
			init();
			AdminCardPanel parent = (AdminCardPanel) getParent();
			CardLayout card = (CardLayout) parent.getLayout();
			((DeckTablePanel) parent.getJpDeck()).init();
			card.show(parent, "Deck");
		}
		
	}
	/**
	 * it makes the background
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("paper1.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
	/**
	 * it changes the police of a JLabel
	 * @param jl
	 */
	public void changePolice(JLabel jl){
		Font fonte = new Font("TimesRoman ",Font.BOLD,20);
		jl.setFont(fonte);
	}
	
	public void editQuestion(Question q){
		edition = q;
		getJtAnswer().setText(q.getAnswer());
		getJtAuthor().setText(q.getAuthor());
		getJtTheme().setText(q.getTheme());
		getJtClue1().setText(q.getClue(0));
		getJtClue2().setText(q.getClue(1));
		getJtClue3().setText(q.getClue(2));
	}
	
	/**
	 * it hide the buttons
	 * @param jb
	 */
	private void hideButton(JButton jb){
		jb.setOpaque(false);
		jb.setContentAreaFilled(false);
		jb.setBorderPainted(false);
	}
	
}
