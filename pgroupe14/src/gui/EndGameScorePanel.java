package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import models.TexturedJButton;


public class EndGameScorePanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton jbScoreboard, jbReplay, jbMenu;
	private JLabel jlTitle, jlPoint;
	
	/**
	 * It allows to position differents elements on the panel
	 */
	public EndGameScorePanel(){
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(true);
		
		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addComponent(getJlTitle(),120,120,120)
				.addComponent(getJlPoint(),30,30,30)
				.addComponent(getJbReplay(),160,160,160)
				.addComponent(getJbScoreboard(),160,160,160)
				.addComponent(getJbMenu(),160,160,160)
				);
		
		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(60)
				.addComponent(getJlTitle())
				.addGap(20)
				.addComponent(getJlPoint())
				.addGap(20)
				.addComponent(getJbReplay(),40,40,40)
				.addGap(15)
				.addComponent(getJbScoreboard(),40,40,40)
				.addGap(15)
				.addComponent(getJbMenu(),40,40,40)
				);
		gl.linkSize(SwingConstants.HORIZONTAL, getJbMenu(), getJbReplay(), getJbScoreboard());
	}
	/**
	 * it allows to send the user on the scoreboard panel
	 * @return
	 * return a jbutton 
	 */
	public JButton getJbScoreboard() {
		if(jbScoreboard == null){
			jbScoreboard = new TexturedJButton("Scoreboard","stone.png");
			jbScoreboard.setForeground(Color.WHITE);
			jbScoreboard.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					goToScoreboard();
				}
			});
		}
		return jbScoreboard;
	}
	/**
	 * it allows to replay the game.
	 * @return
	 * return un jbutton
	 */
	public JButton getJbReplay() {
		if(jbReplay == null){
			jbReplay = new TexturedJButton("Replay","stone.png");
			jbReplay.setForeground(Color.WHITE);
			jbReplay.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					replay();
				}

			});
		}
		return jbReplay;
	}
	/**
	 * it allows to go to the menu
	 * @return
	 * return un jbutton
	 */
	public JButton getJbMenu() {
		if(jbMenu == null){
			jbMenu = new TexturedJButton("Menu","stone.png");
			jbMenu.setForeground(Color.WHITE);
			jbMenu.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					goToMenu();
				}
			});
		}
		return jbMenu;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return un jlabel
	 */
	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new JLabel("Your score :");
			jlTitle.setHorizontalAlignment(SwingConstants.CENTER);
			changeTitle(jlTitle);
		}
		return jlTitle;
	}
	/**
	 * it creates a JLabel (this JLabel represents the point of the user during the game)
	 * @return
	 * Return jlabel
	 */
	public JLabel getJlPoint() {
		if(jlPoint == null){
			jlPoint = new JLabel(""+0);
			jlTitle.setHorizontalAlignment(SwingConstants.CENTER);
			changeTitle(jlPoint);
		}
		return jlPoint;
	}
	/**
	 * It allows to access to the ThemeSelectionPanel
	 */
	private void replay() {
		SinglePlayerCardPanel parent = (SinglePlayerCardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		((ThemeSelectionPanel) parent.getJpThemeSelectionPanel()).resetThemes();
		card.first(parent);
	}
	/**
	 * It allows to access to the main menu
	 */
	private void goToMenu(){
		replay();
		CardPanel parent = (CardPanel) getParent().getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Main Menu");
	}
	/**
	 * It allows to access to the scoreboard
	 */
	private void goToScoreboard(){
		replay();
		CardPanel parent = (CardPanel) getParent().getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Scoreboard");
	}
	/** 
	 * it makes a background 
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("topMountain.jpg").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
	/** 
	 * it allows to change the police of an JLabel
	 */
	public void changeTitle(JLabel jl){
		Font customFont=null;
		try {
			customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Gameplay.ttf")).deriveFont(13f);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		//register the font
		try {
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("Gameplay.ttf")));
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jl.setFont(customFont);
	}


}
