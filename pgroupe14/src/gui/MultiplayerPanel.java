package gui;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MultiplayerPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle;
	private JButton jbBack;

	public MultiplayerPanel(){
		GroupLayout gl=new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);

		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addComponent(getJlTitle())
				.addComponent(getJbBack(),200,200,200)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addComponent(getJlTitle())
				.addComponent(getJbBack(),40,40,40)
				);
	}

	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new JLabel("Multiplayer");
		}
		return jlTitle;
	}

	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new JButton("Back");
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					back();
				}
			});
		}
		return jbBack;
	}



	private void back(){
		CardPanel parent = (CardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Main Menu");
	}
}
