package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import models.TexturedJButton;
import utilities.Synonym;

public class SynonymCreationPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlWord, jlSyns;
	private JTextField jtWord, jtSyn1, jtSyn2, jtSyn3;
	private JButton jbBack, jbSave;
	private List<String> edition;


	/**
	 * It allows to position differents elements on the panel
	 */
	public SynonymCreationPanel(){
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		this.setBackground(Color.black);
		//GROUPE HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGap(800)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlWord(),100,100,100)
						.addComponent(getJtWord(),200,200,200)
						.addGap(70)
						)
				.addComponent(getJlSyns())
				.addComponent(getJtSyn1(),670,670,670)
				.addComponent(getJtSyn2(),670,670,670)
				.addComponent(getJtSyn3(),670,670,670)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbBack(),200,200,200)
						.addGap(60)
						.addComponent(getJbSave(),200,200,200)
						)
				);
		
		//GROUPE VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(40)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlWord())
						.addComponent(getJtWord(),30,30,30)
						)
				.addGap(10)
				.addComponent(getJlSyns())
				.addGap(20)
				.addComponent(getJtSyn1(),30,30,30)
				.addGap(20)
				.addComponent(getJtSyn2(),30,30,30)
				.addGap(20)
				.addComponent(getJtSyn3(),30,30,30)
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbBack(),40,40,40)
						.addComponent(getJbSave(),40,40,40)
						)
				);

	}

	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlTheme
	 */
	public JLabel getJlWord() {
		if(jlWord == null){
			jlWord = new JLabel("Words :");
			changePolice(jlWord);
		}
		return jlWord;
	}

	public JLabel getJlSyns() {
		if(jlSyns == null){
			jlSyns = new JLabel("Synonyms :");
			changePolice(jlSyns);
		}
		return jlSyns;
	}

	
	/**
	 * it creates a JTextField
	 * @return
	 * return a JTextField JtTheme
	 */
	public JTextField getJtWord() {
		if(jtWord == null){
			jtWord = new JTextField();
		}
		return jtWord;
	}
	/**
	 * it creates a JTextField
	 * @return
	 * return a JTextField jtClue1
	 */
	public JTextField getJtSyn1() {
		if(jtSyn1 == null){
			jtSyn1 = new JTextField();
		}
		return jtSyn1;
	}
	/**
	 * it creates a JTextField
	 * @return
	 * return a JTextField jtClu2
	 */
	public JTextField getJtSyn2() {
		if(jtSyn2 == null){
			jtSyn2 = new JTextField();
		}
		return jtSyn2;
	}

	public JTextField getJtSyn3() {
		if(jtSyn3 == null){
			jtSyn3 = new JTextField();
		}
		return jtSyn3;
	}
	/**
	 * it creates a JButton wich allows the user to go on the previous panel
	 * @return
	 * return a JButton
	 */
	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","parcho.png");
			hideButton(jbBack);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					AdminCardPanel parent = (AdminCardPanel) getParent();
					CardLayout card = (CardLayout) parent.getLayout();
					card.show(parent, "Admin option");
				}
			});
		}
		return jbBack;
	}
	/**
	 * it creates a JButton wich save the modifications
	 * @return
	 * return a JButton jbSave
	 */
	public JButton getJbSave() {
		if(jbSave == null){
			jbSave = new TexturedJButton("Save","parcho.png");
			hideButton(jbSave);
			jbSave.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					extractSynonym();
					Synonym.getInstance().toJson("data_synonyms.json");
				}
			});
		}
		return jbSave;
	}
	
	/**
	 * it initializes the JTtextFields
	 */
	public void init(){
		edition=null;
		getJtWord().setText("");
		getJtSyn1().setText("");
		getJtSyn2().setText("");
		getJtSyn3().setText("");
	}
	/**
	 * it allows to exctract the questions created by the Admin
	 */
	private void extractSynonym(){
		boolean testTheme = !getJtWord().getText().equals(""),
				testClues = !getJtSyn1().getText().equals("") || !getJtSyn2().getText().equals("") || !getJtSyn3().getText().equals("");
		if(testTheme && testClues){
			List<String> l = new ArrayList<>();
			l.add(getJtWord().getText());
			if(!getJtSyn1().getText().equals("")){
				l.add(getJtSyn1().getText());
			}
			if(!getJtSyn2().getText().equals("")){
				l.add(getJtSyn2().getText());
			}
			if(!getJtSyn3().getText().equals("")){
				l.add(getJtSyn3().getText());
			}
			
			if(edition!=null){
				Synonym.getInstance().setSynonym(Synonym.getInstance().indexOf(edition), l);
			}else{
				Synonym.getInstance().addSynonym(l);
			}
			init();
			AdminCardPanel parent = (AdminCardPanel) getParent();
			CardLayout card = (CardLayout) parent.getLayout();
			((SynonymTablePanel) parent.getJpSynonym()).init();
			card.show(parent, "Synonym");
		}

	}
	/**
	 * it makes the background
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("paper1.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
	/**
	 * it changes the police of a JLabel
	 * @param jl
	 */
	public void changePolice(JLabel jl){
		Font fonte = new Font("TimesRoman ",Font.BOLD,20);
		jl.setFont(fonte);
	}

	public void editSynonym(List<String> liste){
		edition = liste;
		switch(liste.size()){
		case 4: getJtSyn3().setText(liste.get(3));
		case 3: getJtSyn2().setText(liste.get(2));
		case 2: getJtSyn1().setText(liste.get(1));
		case 1: getJtWord().setText(liste.get(0));
		}
	}
	private void hideButton(JButton jb){
		jb.setOpaque(false);
		jb.setContentAreaFilled(false);
		jb.setBorderPainted(false);
	}
}
