package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import models.TexturedJButton;


public class MainMenuPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton jbSinglePlayer, jbMultiPlayer, jbScoreboard, jbExit, jbOption, jbDisconnection;
	/**
	 * It allows to position differents elements on the panel
	 */
	public MainMenuPanel(){
		//showAdminBoutton();
		this.setOpaque(false);
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(true);


		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addComponent(getJbSinglePlayer(),200,200,200)
				.addComponent(getJbMultiPlayer(),200,200,200)
				.addComponent(getJbScoreboard(),200,200,200)
				.addComponent(getJbOption(),200,200,200)
				.addComponent(getJbDisconnection(), 200,200,200)
				.addComponent(getJbExit(),200,200,200)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(150)
				.addComponent(getJbSinglePlayer(),40,40,40)
				.addComponent(getJbMultiPlayer(),40,40,40)
				.addComponent(getJbScoreboard(),40,40,40)
				.addComponent(getJbOption(),40,40,40)
				.addComponent(getJbDisconnection(),40,40,40)
				.addComponent(getJbExit(),40,40,40)
				);

		gl.linkSize(SwingConstants.HORIZONTAL, getJbSinglePlayer(), getJbMultiPlayer(), getJbScoreboard(), getJbExit(),getJbOption());
	}
	/**
	 * it allows to create a jbutton wich is used to access to the menu Single Player
	 * @return the JButton
	 */
	public JButton getJbSinglePlayer() {
		if(jbSinglePlayer == null){
			jbSinglePlayer = new TexturedJButton("Single Player","stone.png");
			jbSinglePlayer.setForeground(Color.WHITE);
			//jbSinglePlayer.setVisible(false);
			jbSinglePlayer.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					buttonPressed(arg0);
					showSinglePlayer();
				}

			});
		}
		return jbSinglePlayer;
	}
	/**
	 * it allows to created a jbutton wich is used to access to the multiplayermenu
	 * @return a JButton
	 */
	public JButton getJbMultiPlayer() {
		if(jbMultiPlayer == null){
			jbMultiPlayer = new TexturedJButton("Multiplayer","stone.png");
			jbMultiPlayer.setForeground(Color.WHITE);
			jbMultiPlayer.setVisible(false);
			jbMultiPlayer.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					buttonPressed(arg0);
				}

			});
		}
		return jbMultiPlayer;
	}

	/**
	 * it allows to create a jbutton wich is used to access to the scoreboard
	 * @return a JButton
	 */
	public JButton getJbScoreboard() {
		if(jbScoreboard == null){
			jbScoreboard = new TexturedJButton("Scoreboard","stone.png");
			jbScoreboard.setForeground(Color.WHITE);
			jbScoreboard.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					buttonPressed(arg0);
				}

			});
		}
		return jbScoreboard;
	}
	/**
	 * it allows to create a jbutton wich is used to access to the window
	 * @return a JButton
	 */
	public JButton getJbExit() {
		if(jbExit == null){
			jbExit = new TexturedJButton("Exit","stone.png");
			jbExit.setForeground(Color.WHITE);
			jbExit.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					System.exit(0);
				}
			});
		}
		return jbExit;
	}

	public JButton getJbOption() {
		if(jbOption == null){
			jbOption = new TexturedJButton("Option","stone.png");
			jbOption.setForeground(Color.WHITE);
			jbOption.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					buttonPressed(arg0);
				}
			});
		}
		return jbOption;
	}
	/**
	 * It allows to disconnect from the application
	 */
	public JButton getJbDisconnection() {
		if(jbDisconnection == null){
			jbDisconnection = new TexturedJButton("Log out","stone.png");
			jbDisconnection.setForeground(Color.WHITE);
			jbDisconnection.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					CardPanel parent = (CardPanel) getParent();
					CardLayout card = (CardLayout) parent.getLayout();
					card.first(parent);
					parent.setPlayer(null);
				}
			});
		}
		return jbDisconnection;
	}
	/**
	 * it allows to display the menu depending on the differents buttons
	 * @param arg0 ActionEvent d�clanch�
	 */
	private void buttonPressed(ActionEvent arg0) {
		JButton o = (JButton) arg0.getSource();
		CardPanel parent = (CardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, o.getText());
	}
	/**
	 * It allows to display the singlePlayer menu wich have initialized the theme randomly
	 */
	private void showSinglePlayer() {
		CardPanel parent = (CardPanel) getParent();
		SinglePlayerCardPanel sp = (SinglePlayerCardPanel) parent.getJpSinglePlayer();

		((ThemeSelectionPanel) sp.getJpThemeSelectionPanel()).resetThemes();
		
		//if(sp!=null){
			((ThemeSelectionPanel) sp.getJpThemeSelectionPanel()).resetThemes();
	//	}else{
			CardPanel parent1 = (CardPanel) this.getParent();
			CardLayout card = (CardLayout) parent.getLayout();
			card.show(parent1, "jpSinglePlayer");
		//}
	}
	/**
	 * it allows to show the adminButton
	 */
	void showAdminBoutton(){
		CardPanel parent = (CardPanel) getParent();
		boolean visibility = !parent.getPlayer().isAdmin();
		getJbSinglePlayer().setVisible(visibility);
	}

}
