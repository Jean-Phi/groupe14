package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import models.GestionPersonne;
import models.TexturedJButton;
import models.User;

public class SubscribePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlUserName, jlPassword, jlConfirm, jlMessageUser,jlMessagePassword,jlMessageConfirm;
	private JTextField jtUserName;
	private JPasswordField jpPassword, jpConfirm;
	private JButton jbSubscribe, jbCancel;

	/**
	 * It allows to position differents elements on the panel
	 */
	public SubscribePanel(){
		this.setOpaque(false);
		this.setPreferredSize(new Dimension(800,450));
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(true);

		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(1000))
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlUserName(),145,145,145)
						.addComponent(getJtUserName(),320,320,320)
						.addComponent(getJlMessageUser(),250,250,250)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlPassword(),145,145,145)
						.addComponent(getJpPassword(),320,320,320)
						.addComponent(getJlMessagePassword(),250,250,250)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlConfirm(),145,145,145)
						.addComponent(getJpConfirm(),320,320,320)
						.addComponent(getJlMessageConfirm(),250,250,250)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbCancel(),100,100,100)
						.addComponent(getJbSubscribe(),100,100,100)
						)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(160)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlUserName())
						.addComponent(getJtUserName())	
						.addComponent(getJlMessageUser())
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlPassword())
						.addComponent(getJpPassword())
						.addComponent(getJlMessagePassword())
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlConfirm())
						.addComponent(getJpConfirm())
						.addComponent(getJlMessageConfirm())
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbCancel())
						.addComponent(getJbSubscribe())			
						)
				);
		gl.linkSize(SwingConstants.VERTICAL, getJlUserName(),getJtUserName(),getJpPassword(), getJpConfirm());

	}
	/**
	 * create a button that cancels and bring the user to the previous panel
	 * @return
	 */
	public JButton getJbCancel() {
		if(jbCancel==null){
			jbCancel=new TexturedJButton("Cancel","stone.png");
			jbCancel.setForeground(Color.WHITE);
			jbCancel.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					cancel();
				}
			});
		}
		return jbCancel;
	}


	/**
	 * create a button wich allows the user to submit his subscribe
	 * @return a JButton
	 */
	public JButton getJbSubscribe() {
		if(jbSubscribe==null){
			jbSubscribe=new TexturedJButton("Submit","stone.png");
			jbSubscribe.setForeground(Color.WHITE);
			jbSubscribe.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					submit();
				}
			});
		}
		return jbSubscribe;
	}

	/**
	 * Create a JLabel
	 * @return
	 * return the JLabel JLUsername
	 */
	public JLabel getJlUserName() {
		if(jlUserName == null){
			jlUserName = new JLabel("Username : ");
			jlUserName.setHorizontalAlignment(SwingConstants.RIGHT);
			changeTitle(jlUserName);
		}
		return jlUserName;
	}
	/**
	 * Create a JLabel
	 * @return
	 * return the JLabel JlPassword
	 */
	public JLabel getJlPassword() {
		if(jlPassword == null){
			jlPassword = new JLabel("Password : ");
			jlPassword.setHorizontalAlignment(SwingConstants.RIGHT);
			changeTitle(jlPassword);
		}
		return jlPassword;
	}
	/**
	 * Create a JLabel
	 * @return
	 * return the JLabel JLMessageUser
	 */
	public JLabel getJlMessageUser() {
		if(jlMessageUser==null){
			jlMessageUser = new JLabel(" ");
			jlMessageUser.setHorizontalTextPosition(SwingConstants.CENTER);
			jlMessageUser.setForeground(Color.red);
		}
		return jlMessageUser;
	}
	/**
	 * Create a JLabel
	 * @return
	 * return the JLabel JLMessagePassword
	 */
	public JLabel getJlMessagePassword() {
		if(jlMessagePassword==null){
			jlMessagePassword = new JLabel(" ");
			jlMessagePassword.setHorizontalTextPosition(SwingConstants.CENTER);
			jlMessagePassword.setForeground(Color.red);
		}
		return jlMessagePassword;
	}
	/**
	 * Create a JLabel
	 * @return
	 * return the JLabel JLMessageConfirm
	 */
	public JLabel getJlMessageConfirm() {
		if(jlMessageConfirm==null){
			jlMessageConfirm = new JLabel(" ");
			jlMessageConfirm.setHorizontalTextPosition(SwingConstants.CENTER);
			jlMessageConfirm.setForeground(Color.red);
		}
		return jlMessageConfirm;
	}

	/**
	 * Create a JLabel
	 * @return
	 * return the JTextField JtUsername
	 */
	public JTextField getJtUserName() {
		if(jtUserName == null){
			jtUserName = new JTextField("");
		}
		return jtUserName;
	}
	/**
	 * Create a JPasswordField
	 * @return
	 * return the JPasswordField
	 */
	public JPasswordField getJpPassword() {
		if(jpPassword == null){
			jpPassword = new JPasswordField("");
		}
		return jpPassword;
	}
	/**
	 * Create a JLabel
	 * @return
	 * Return the JLabel jlConfirm
	 */

	public JLabel getJlConfirm() {
		if(jlConfirm == null){
			jlConfirm = new JLabel("Confirm    : ");
			jlConfirm.setHorizontalAlignment(SwingConstants.RIGHT);
			changeTitle(jlConfirm);
		}
		return jlConfirm;
	}
	/**
	 * Create a JPasswordField
	 * @return
	 * return the JLabel JpConfirm
	 */
	public JPasswordField getJpConfirm() {
		if(jpConfirm == null){
			jpConfirm = new JPasswordField();
		}
		return jpConfirm;
	}

	/**
	 * Allows to identify (pas encore impl�ment�)
	 * We go on the next panel
	 */
	private void submit(){
		init();
		String message = "";
		boolean testPassword = !String.valueOf(getJpPassword().getPassword()).equals(""),
				testUserName = !getJtUserName().getText().equals(""),
				testConfirm = !String.valueOf(getJpConfirm().getPassword()).equals("");
		if(testPassword && testUserName && testConfirm){
			if(Arrays.equals(getJpConfirm().getPassword(), getJpPassword().getPassword())){
				User u = new User(getJtUserName().getText(), String.valueOf(getJpPassword().getPassword()));
				if(!GestionPersonne.getInstance().getUsers().containsKey(getJtUserName().getText())){
					GestionPersonne.getInstance().addUser(u);
					cancel();
				}else{
					/*user dej� existant*/
					message = "User already existing";
					getJlMessageUser().setText(message);	
				}
			}else{
				/*mots de passe diff�rents*/
				message ="Password are not the same";
				getJlMessagePassword().setText(message);
				getJlMessageConfirm().setText(message);
			}
		}else{
			/*champs vide*/
			message ="Empty fields";
			if(!testPassword){
				getJlMessagePassword().setText(message);
			}
			if(!testUserName){
				getJlMessageUser().setText(message);
			}
			if(!testConfirm){
				getJlMessageConfirm().setText(message);
			}
		}
		getJlMessageUser().repaint();
		getJlMessagePassword().repaint();
		getJlMessageConfirm().repaint();
	}
	/**
	 * it allows the user to go to the previous panel
	 */
	private void cancel(){
		CardPanel parent = (CardPanel) this.getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		init();
		card.show(parent,"LoginPanel");
	}
	/**
	 * initialize the JLabels
	 */
	private void init(){
		getJlMessageUser().setText("");
		getJlMessagePassword().setText("");
		getJlMessageConfirm().setText("");
	}
	private void changeTitle(JLabel jl){
		Font customFont=null;
		try {
			customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Gameplay.ttf")).deriveFont(10f);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		//register the font
		try {
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("Gameplay.ttf")));
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jl.setFont(customFont.deriveFont(customFont.getStyle()| Font.BOLD));
		jl.setForeground(Color.WHITE);
		
	}
}

