package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import models.GestionPersonne;
import models.TexturedJButton;
import models.User;

public class UserCreationPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle, jlNickname, jlPassword, jlAdmin;
	private JTextField jtNickname, jtPassword;
	private JCheckBox jcAdmin;
	private JButton jbBack, jbSave;

	/**
	 * It allows to position differents elements on the panel
	 */
	public UserCreationPanel(){
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		this.setBackground(Color.BLACK);
		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGap(800)
				.addComponent(getJlTitle())
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlNickname(),105,105,105)
						.addComponent(getJtNickname(),560,560,560)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlPassword(),105,105,105)
						.addComponent(getJtPassword(),560,560,560)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlAdmin(),100,100,100)
						.addComponent(getJcAdmin())
						)
				.addGap(200)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbBack(),200,200,200)
						.addGap(30)
						.addComponent(getJbSave(),200,200,200)
						)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(120)
				.addComponent(getJlTitle())
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlNickname())
						.addComponent(getJtNickname(),30,30,30)
						)
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlPassword())
						.addComponent(getJtPassword(),30,30,30)
						)
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlAdmin())
						.addComponent(getJcAdmin(),30,30,30)
						)
				.addGap(40)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbSave(),40,40,40)
						.addComponent(getJbBack(),40,40,40)
						)
				);
		gl.linkSize(SwingConstants.VERTICAL,getJtNickname(), getJtPassword());
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlTitle
	 */
	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new JLabel("User creation");
			changePolice(jlTitle);
		}
		return jlTitle;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlNickname
	 */
	public JLabel getJlNickname() {
		if(jlNickname == null){
			jlNickname = new JLabel("Nickname");
			changePolice(jlNickname);
		}
		return jlNickname;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlPassword
	 */
	public JLabel getJlPassword() {
		if(jlPassword == null){
			jlPassword = new JLabel("Password");
			changePolice(jlPassword);
		}
		return jlPassword;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlAdmin
	 */
	public JLabel getJlAdmin() {
		if(jlAdmin == null){
			jlAdmin = new JLabel("Admin");
			changePolice(jlAdmin);
		}
		return jlAdmin;
	}
	/**
	 * it creates a JTextField
	 * @return
	 * return a Jlabel jtNicknames
	 */
	public JTextField getJtNickname() {
		if(jtNickname == null){
			jtNickname = new JTextField();
		}
		return jtNickname;
	}
	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jtPassword
	 */
	public JTextField getJtPassword() {
		if(jtPassword == null){
			jtPassword = new JTextField();
		}
		return jtPassword;
	}
	/**
	 * it creates a JCheckBox
	 * @return
	 * return a JCheckBow JcAdmin 
	 */
	public JCheckBox getJcAdmin() {
		if(jcAdmin == null){
			jcAdmin = new JCheckBox();
			jcAdmin.setOpaque(false);
		}
		return jcAdmin;
	}
	/**
	 * it creates a JButton
	 * @return
	 * return a Jlabel jbBack
	 */
	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","parcho.png");
			hideButton(jbBack);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					AdminCardPanel parent = (AdminCardPanel) getParent();
					CardLayout card = (CardLayout) parent.getLayout();
					card.show(parent, "Admin option");
				}
			});
		}
		return jbBack;
	}
	/**
	 * it creates a JButton
	 * @return
	 * return a Jlabel jbSave
	 */
	public JButton getJbSave() {
		if(jbSave == null){
			jbSave = new TexturedJButton("Save","parcho.png");
			hideButton(jbSave);
			jbSave.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					extractUser();
					GestionPersonne.getInstance().toJson("Users.json");
				}
			});
		}
		return jbSave;
	}
	/**
	 * it initializes the JTextfields and the JCheckBox
	 */
	private void init(){
		getJtNickname().setText("");
		getJtPassword().setText("");
		getJcAdmin().setSelected(false);
	}
	/**
	 * it extracts the user from the JPanel
	 */
	private void extractUser(){
		boolean testPassword = !String.valueOf(getJtPassword().getText()).equals(""),
				testUserName = !getJtNickname().getText().equals("");
		if(testPassword && testUserName){
			User u = new User(getJtNickname().getText(), getJtPassword().getText(), getJcAdmin().isSelected());
			GestionPersonne.getInstance().addUser(u);
			AdminCardPanel parent = (AdminCardPanel) getParent();
			CardLayout card = (CardLayout) parent.getLayout();
			card.show(parent, "Users");
			init();
		}
	}
	/**
	 * it makes a background
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("paper1.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
	/**
	 * it changes the police of the JLabel
	 * @param jl
	 */
	public void changePolice(JLabel jl){
		Font fonte = new Font("TimesRoman ",Font.BOLD,20);
		jl.setFont(fonte);
	}
	private void hideButton(JButton jb){
		jb.setOpaque(false);
		jb.setContentAreaFilled(false);
		jb.setBorderPainted(false);
	}
}
