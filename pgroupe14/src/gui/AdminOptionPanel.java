package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import models.TexturedJButton;
import models.TexturedJLabel;


public class AdminOptionPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle;
	private JButton jbBack, jbUsers, jbDeck, jbSynonym, jbCreatUser, jbCreatQuestion,  jbCreateSynonym;
	/**
	 * Create the panel of AdminOption
	 */
	public AdminOptionPanel(){
		GroupLayout gl=new GroupLayout(this);
		this.setLayout(gl);
		this.setBackground(Color.BLACK);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);
		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addComponent(getJlTitle(),200,200,200)
				.addGroup(gl.createSequentialGroup()
						.addGroup(gl.createParallelGroup()
								.addComponent(getJbDeck(),200,200,200)
								.addComponent(getJbCreatQuestion(),200,200,200)
								)
						.addGroup(gl.createParallelGroup()
								.addComponent(getJbUsers(),200,200,200)
								.addComponent(getJbCreatUser(),200,200,200)
								)
						.addGroup(gl.createParallelGroup()
								.addComponent(getJbSynonym(),200,200,200)
								.addComponent(getJbCreateSynonym(),200,200,200)
								)
						)
				.addComponent(getJbBack(),200,200,200)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(20)
				.addComponent(getJlTitle(),60,60,60)
				.addGap(40)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbDeck(),60,60,60)
						.addComponent(getJbUsers(),60,60,60)
						.addComponent(getJbSynonym(),60,60,60)
						)
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbCreatQuestion(),60,60,60)
						.addComponent(getJbCreatUser(),60,60,60)
						.addComponent(getJbCreateSynonym(),60,60,60)
						)
				.addGap(40)
				.addComponent(getJbBack(),60,60,60)
				);
	}
	/**
	 * It allows to create a JLabel
	 * @return
	 * Return the JLabel of the title
	 */
	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new TexturedJLabel("Admin Option","Parchemin.png");
		}
		return jlTitle;
	}	
	/**
	 *  It allows the Admin to go back on the menu.
	 * @return
	 * Return the JButton back.
	 */
	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","parcho.png");
			hideButton(jbBack);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					back();
				}
			});
		}
		return jbBack;
	}
	/**
	 * It allows the Admin to go to the UserTablePanel 
	 * @return
	 * Return the JButton users.
	 */
	public JButton getJbUsers() {
		if(jbUsers == null){
			jbUsers = new TexturedJButton("Users","parcho.png");
			hideButton(jbUsers);
			jbUsers.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					buttonPressed(e);
				}

			});
		}
		return jbUsers;
	}
	/**
	 * It allows the Admin to go to the DeckTablePanel
	 * @return
	 * Return the JButton Deck
	 */
	public JButton getJbDeck() {
		if(jbDeck == null){
			jbDeck = new TexturedJButton("Deck","parcho.png");
			hideButton(jbDeck);
			jbDeck.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					buttonPressed(e);
				}

			});
		}
		return jbDeck;
	}
	/**
	 * It allows the Admin to go to the UserCreationTablePanel
	 * @return
	 * Return the JButton createUser.
	 */
	public JButton getJbCreatUser() {
		if(jbCreatUser == null){
			jbCreatUser = new TexturedJButton("Create user","parcho.png");
			hideButton(jbCreatUser);
			jbCreatUser.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					buttonPressed(e);
				}
			});
		}
		return jbCreatUser;
	}
	/**
	 * It allows the Admin to go to the QuestionCreationTablePanel
	 * @return
	 * Return the JButton createQuestion.
	 */
	public JButton getJbCreatQuestion() {
		if(jbCreatQuestion == null){
			jbCreatQuestion = new TexturedJButton("Create question","parcho.png");
			hideButton(jbCreatQuestion);
			jbCreatQuestion.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					buttonPressed(e);
				}
			});
		}
		return jbCreatQuestion;
	}

	public JButton getJbSynonym() {
		if(jbSynonym == null){
			jbSynonym = new TexturedJButton("Synonym","parcho.png");
			hideButton(jbSynonym);
			jbSynonym.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					buttonPressed(e);
				}
			});
		}
		return jbSynonym;
	}
	public JButton getJbCreateSynonym() {
		if(jbCreateSynonym == null){
			jbCreateSynonym = new TexturedJButton("Create synonym","parcho.png");
			hideButton(jbCreateSynonym);
			jbCreateSynonym.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					buttonPressed(e);
				}
			});
		}
		return jbCreateSynonym;
	}
	/**
	 * It allows the admin to go to the OptionPanel
	 */
	private void back(){
		CardPanel parent = (CardPanel) getParent().getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Option");
	}
	/**
	 * It allows the Admin to access to the UserCreationTablePanel
	 * @return
	 * Return the JButton createUser.
	 */
	private void buttonPressed(ActionEvent arg0) {
		JButton o = (JButton) arg0.getSource();
		AdminCardPanel parent = (AdminCardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, o.getText());
		if(o.getText().equals("Create question")){
			((QuestionCreationPanel) parent.getJpQuestionCreation()).init();
		}
		if(o.getText().equals("Deck")){
			((DeckTablePanel) parent.getJpDeck()).init();
		}
	}
	/**
	 * It puts a background 
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("paper.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
	/**
	 * It makes the button invisible
	 */
	private void hideButton(JButton jb){
		jb.setOpaque(false);
		jb.setContentAreaFilled(false);
		jb.setBorderPainted(false);
		//jb.setHorizontalAlignment(SwingConstants.CENTER);
		//jb.setVerticalAlignment(SwingConstants.CENTER);
	}
}
