package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;

import models.ChronoBar;
import models.Deck;
import models.HighScore;
import models.User;
import models.Question;
import models.Score;
import models.TexturedJButton;
import utilities.Recognition;

public class GamePanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static int timeLimit = 80;

	private ChronoBar chronoBar;
	private JLabel jlThemeSelected, jlPoint, jlTime;
	private JTextPane jt1,jt2,jt3;
	private JButton jbNext;
	private JTextField jtAnswer;
	private Timer timerClues;
	private int time;
	private int courentPoint;
	private ImageIcon previousImage;
	private Question qActual;
	private List<Question> questions;
	private int indexQuestion, point;

	/**
	 * Constructor
	 * It allows to position differents elements on the panel
	 */
	public GamePanel(){
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(true);

		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createSequentialGroup()
				.addGap(150)
				.addGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
						.addGap(200)
						.addGroup(gl.createSequentialGroup()
								.addGap(40)
								.addComponent(getJlPoint())
								.addGap(0,0,Short.MAX_VALUE)
								.addComponent(getChronoBar())
								)
						.addComponent(getJlThemeSelected(),250,250,250)
						.addComponent(getJt1(),400,400,400)
						.addComponent(getJt2(),400,400,400)
						.addComponent(getJt3(),400,400,400)
						.addGroup(gl.createSequentialGroup()
								.addComponent(getJtAnswer(),400,400,400)
								.addComponent(getJbNext())
								)
						)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createParallelGroup()
				.addComponent(getChronoBar())
				.addGroup(gl.createSequentialGroup()
						.addGroup(gl.createParallelGroup()
								.addComponent(getJlPoint())
								.addComponent(getJlThemeSelected())
								)
						.addGap(10)
						.addComponent(getJt1(),90,90,90)
						.addComponent(getJt2(),90,90,90)
						.addComponent(getJt3(),90,90,90)
						.addGap(10)
						.addGroup(gl.createParallelGroup()
								.addComponent(getJtAnswer())
								.addComponent(getJbNext())
								)
						)
				);
		gl.linkSize(SwingConstants.VERTICAL, getJbNext(), getJtAnswer());
	}

	/**
	 * it creates a JLabel
	 * @return
	 * return a Jlabel jlTheme
	 */
	public JLabel getJlThemeSelected() {
		if(jlThemeSelected==null){
			jlThemeSelected = new JLabel("Theme selected");
			changePolice(null, jlThemeSelected);
		}
		return jlThemeSelected;
	}

	/**
	 * it creates a JLabel
	 * @return
	 * return a jlabel jlPoint
	 */
	public JLabel getJlPoint() {
		if(jlPoint==null){
			jlPoint = new JLabel(""+point);
			changePolice(null, jlPoint);
		}
		return jlPoint;
	}

	/**
	 * it creates a JLabel
	 * @return
	 * return a jlabel jlTime
	 */
	public JLabel getJlTime() {
		if(jlTime==null){
			time=timeLimit;
			jlTime = new JLabel(""+time);
			changePolice(null, jlTime);
		}
		return jlTime;
	}

	/**
	 * it creates an JTextPane
	 * @return
	 * return a JtexPane jt1
	 */
	public JTextPane getJt1() {
		if(jt1==null){
			jt1=new JTextPane();
			invisible(jt1);
			jt1.setAlignmentX(SwingConstants.CENTER);
			changePolice(jt1,null);
		}
		return jt1;
	}

	/**
	 * it creates an JTextPane
	 * @return
	 * return a JtexPane jt2
	 */
	public JTextPane getJt2() {
		if(jt2==null){
			jt2=new JTextPane();
			invisible(jt2);
			changePolice(jt2,null);
		}
		return jt2;
	}

	/**
	 * it creates an JTextPane
	 * @return
	 * return a JtexPane jt3
	 */
	public JTextPane getJt3() {
		if(jt3==null){
			jt3=new JTextPane();
			invisible(jt3);
			changePolice(jt3,null);
		}
		return jt3;
	}

	/**
	 * It create a JTextField
	 * It allows the user to skip the question
	 * @return
	 * return a jButton jbNext
	 */
	public JButton getJbNext() {
		if(jbNext == null){
			jbNext = new TexturedJButton("Next","stone.png");
			jbNext.setForeground(Color.white);
			jbNext.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					resetCourentPoint();
					nextQuestion();
				}
			});
		}
		return jbNext;
	}

	/**
	 * It create a JTextField
	 * When the user click on "enter" it checks the user's answer
	 * If it's correct we give him a point also he lose his point.
	 * @return
	 * return a jTextfield jtAnswer
	 */
	public JTextField getJtAnswer() {
		if(jtAnswer == null){
			jtAnswer = new JTextField();
			jtAnswer.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					if(Recognition.recognize(qActual.getAnswer(), jtAnswer.getText())){
						increaseCourentPoint();
					}else{
						resetCourentPoint();
					}
					nextQuestion();
				}
			});
		}
		return jtAnswer;
	}

	public ChronoBar getChronoBar() {
		if(chronoBar == null){
			chronoBar = new ChronoBar(timeLimit);
		}
		return chronoBar;
	}

	/**
	 * Create a timer which allows to stop the game if the timer reach 0
	 * @return
	 * Return the state of the timer
	 */
	public Timer getTimerClues() {
		if(timerClues == null){
			timerClues = new Timer();
			timerClues.schedule(new TimerTask() {
				@Override
				public void run() {
					showClues();
				}
			}, Recognition.getDifficult().getDelay()*1000, Recognition.getDifficult().getDelay()*1000);
		}
		return timerClues;
	}

	/**
	 * It allows to show the clues of the questions.
	 */
	private void showClues(){
		if (getJt2().getText().equals(" ")) {
			getJt2().setText(qActual.getClue(1));
			getJt2().repaint();
			return;
		}
		if(!getJt2().getText().equals(" ")&& getJt3().getText().equals(" ")){
			getJt3().setText(qActual.getClue(2));
			getJt3().repaint();
			return;
		}
	}

	/**
	 * Send the user to the next panel when the time reaches 0 or when there is no questions.
	 */
	public void stop() {
		getTimerClues().cancel();
		SinglePlayerCardPanel parent = (SinglePlayerCardPanel) getParent();
		CardLayout card = (CardLayout)parent.getLayout();
		card.next(parent);
		CardPanel cardPanel = (CardPanel) parent.getParent();
		((EndGameScorePanel)parent.getJpEndGameScorePanel()).getJlPoint().setText(""+cardPanel.getPlayer().getScore());
		User pl = cardPanel.getPlayer();
		Score s= new Score(pl.getNickname(),questions.get(0).getTheme(),pl.getScore(),Recognition.getDifficult());
		if(!pl.getNickname().equals("guest")){
			HighScore.getInstance().addScore(s);
		}
	}

	/**
	 * It allows to initialize the list of question with the selectioned theme,the chrono and the points
	 * @param theme
	 * the selectioned theme on the previous panel
	 */
	public void start(String theme) {
		courentPoint=0;
		getJlPoint().setText(""+courentPoint);
		getJlPoint().repaint();
		getJtAnswer().setText("");
		CardPanel parent = (CardPanel) getParent().getParent();
		parent.getPlayer().resetScore();
		time=Recognition.getDifficult().getTime();
		getChronoBar().reset(time);
		getChronoBar().start();
		//Reset des questions
		indexQuestion=0;
		questions=Deck.getInstance().getDeckTheme(theme);
		Collections.shuffle(questions);
		qActual = questions.get(indexQuestion);
		getJlThemeSelected().setText(theme);
		setClues();
		getTimerClues();
		//DEMO
		System.out.println(qActual.getAnswer());
	}

	/**
	 * It allows to skip the question
	 */
	private void nextQuestion(){
		getTimerClues().cancel();
		timerClues=null;
		getTimerClues();
		getJtAnswer().setText("");
		if(indexQuestion<questions.size()-1){
			indexQuestion++;
			qActual=questions.get(indexQuestion);
			setClues();
		}else{
			getChronoBar().reset(0);
			//stop();
			getTimerClues().cancel();
		}
		System.out.println(qActual.getAnswer());
	}

	/**
	 * initialize the clues
	 */
	private void setClues(){
		getJt1().setText(qActual.getClue(0));
		getJt2().setText(" ");
		getJt3().setText(" ");
	}

	/**
	 * it makes a background
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		int i;
		Image image = new ImageIcon("waiting.gif").getImage();
		if(courentPoint>0){
			image = new ImageIcon(courentPoint+"point.gif").getImage();
		}
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
		for(i=1;i<=courentPoint;i++){
			ImageIcon image1= new ImageIcon(i-1+"point.gif");
			swapImage(image1);
		}
		getJlTime().repaint();
	}

	/**
	 * it allows to reset the courentPoint 
	 */
	private void resetCourentPoint(){
		ImageIcon previousImage=new ImageIcon(courentPoint+"point.gif");
		swapImage(previousImage);
		courentPoint=0;
		getJlPoint().setText(""+courentPoint);
		getJlPoint().repaint();
		this.repaint();
	}

	/**
	 * it allows to increase the courentPoint
	 */
	private void increaseCourentPoint(){
		User pl = ((CardPanel) getParent().getParent()).getPlayer();
		if(++courentPoint>=pl.getScore()){
			pl.setScore(courentPoint);
		}
		getJlPoint().setText(""+courentPoint);
		getJlPoint().repaint();
		this.repaint();
	}

	/**
	 * it allows to make the background of the JTextPane invisible 
	 * it allows to forbid to edit the JTextPane
	 */
	private void invisible(JTextPane jt){
		jt.setEditable(false);
		jt.setOpaque(false);
		jt.setForeground(Color.LIGHT_GRAY);
	}

	public void swapImage(ImageIcon image) {
		previousImage=null;
		image.getImage().flush();
		previousImage=image;
	}
	public ImageIcon getPreviousImage() {
		return previousImage;
	}

	public void setPreviousImage(ImageIcon previousImage) {
		this.previousImage = previousImage;
	}
	public void changePolice(JTextPane jt,JLabel jl){
		Font customFont=null;
		try {
			customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Sarpanch-SemiBold.otf")).deriveFont(18f);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		//register the font
		try {
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("Sarpanch-SemiBold.otf")));
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(jt!=null){
		jt.setFont(customFont);
		jt.setForeground(Color.LIGHT_GRAY);
		}else{
			jl.setFont(customFont);
			jl.setForeground(Color.LIGHT_GRAY);
		}

	}

}
