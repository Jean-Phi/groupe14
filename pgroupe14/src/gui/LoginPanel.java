package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import models.GestionPersonne;
import models.TexturedJButton;

public class LoginPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlUserName, jlPassword, jlMessageUsername, jlMessagePassword;
	private JTextField jtUserName;
	private JPasswordField jpPassword;
	private JButton jbConnection, jbSubscribe,jbGuest;

	/**
	 * It allows to position differents elements on the panel
	 */
	public LoginPanel(){
		this.setOpaque(false);
		this.setPreferredSize(new Dimension(800,450));
		GroupLayout gl = new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(true);

		//TEST
		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlUserName(),145,145,145)
						.addComponent(getJtUserName(),320,320,320)
						.addComponent(getJlMessageUsername(),100,100,100)
						)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlPassword(),145,145,145)
						.addComponent(getJpPassword(),320,320,320)
						.addComponent(getJlMessagePassword(),100,100,100)
						)
				.addGroup(gl.createSequentialGroup()
						.addGap(45)
						.addComponent(getJbGuest(),110,110,110)
						.addComponent(getJbSubscribe(),100,100,100)
						.addComponent(getJbConnection(),100,100,100)
						)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGap(180)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlUserName())
						.addComponent(getJtUserName())
						.addComponent(getJlMessageUsername())
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlPassword())
						.addComponent(getJpPassword())
						.addComponent(getJlMessagePassword())
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbGuest())
						.addComponent(getJbConnection())
						.addComponent(getJbSubscribe())
						)
				);
		gl.linkSize(SwingConstants.VERTICAL, getJlUserName(),getJtUserName(),getJpPassword());

	}
	/**
	 *  It allows the user to subscribe
	 * @return
	 * Return the JButton subscribe.
	 */
	public JButton getJbSubscribe() {
		if(jbSubscribe==null){
			jbSubscribe=new TexturedJButton("Subscribe","stone.png");
			jbSubscribe.setForeground(Color.WHITE);
			jbSubscribe.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					subscribe();
				}

			});
		}
		return jbSubscribe;
	}

	/**
	 * Create the JLabelUserName
	 * @return
	 * return the JLabel 
	 */
	public JLabel getJlUserName() {
		if(jlUserName == null){
			jlUserName = new JLabel("Username : ");
			jlUserName.setHorizontalAlignment(SwingConstants.RIGHT);
			changeTitle(jlUserName);
		}
		return jlUserName;
	}
	/**
	 * Create the JLabelPassword
	 * @return
	 * return the JLabel 
	 */
	public JLabel getJlPassword() {
		if(jlPassword == null){
			jlPassword = new JLabel("Password : ");
			jlPassword.setHorizontalAlignment(SwingConstants.RIGHT);
			changeTitle(jlPassword);
		}
		return jlPassword;
	}
	/**
	 * Create the JLabelMessageUsername
	 * @return
	 * return the JLabel 
	 */
	public JLabel getJlMessageUsername() {
		if(jlMessageUsername==null){
			jlMessageUsername = new JLabel("");
			jlMessageUsername.setHorizontalTextPosition(SwingConstants.CENTER);
			jlMessageUsername.setForeground(Color.red);
		}
		return jlMessageUsername;
	}
	/**
	 * Create the JLabelMessagePassword
	 * @return
	 * return the JLabel 
	 */
	public JLabel getJlMessagePassword() {
		if(jlMessagePassword==null){
			jlMessagePassword = new JLabel("");
			jlMessagePassword.setHorizontalTextPosition(SwingConstants.CENTER);
			jlMessagePassword.setForeground(Color.red);
		}
		return jlMessagePassword;
	}

	/**
	 * Create the JTextFieldUsername
	 * @return
	 * return the JTextField
	 */
	public JTextField getJtUserName() {
		if(jtUserName == null){
			jtUserName = new JTextField("admin");
		}
		return jtUserName;
	}
	/**
	 * Cr�er un JTextfield
	 * @return
	 * return le JTextField du mot de passe
	 */
	public JPasswordField getJpPassword() {
		if(jpPassword==null){
			jpPassword=new JPasswordField("helha");
		}
		return jpPassword;
	}

	/**
	 * Create the JButton connection
	 * @return
	 * Return the JButton
	 */
	public JButton getJbConnection() {
		if(jbConnection == null){
			jbConnection = new TexturedJButton("Log in","stone.png");
			jbConnection.setForeground(Color.WHITE);
			jbConnection.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					connection();
				}

			});
		}
		return jbConnection;
	}


	/**
	 * It allows to identify and to skip on the next panel
	 */
	private void connection(){
		resetMessage();
		CardPanel parent = (CardPanel) this.getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		String message ="";
		try {
			if(GestionPersonne.getInstance().getUser(getJtUserName().getText()).getPassword().equals(String.valueOf(getJpPassword().getPassword()))&& !(GestionPersonne.getInstance().getUser(getJtUserName().getText()).getPassword().equals("")) ){
				parent.setPlayer(GestionPersonne.getInstance().getUser(getJtUserName().getText()));
				card.show(parent,"Main Menu");
				((OptionPanel)parent.getJpOption()).showAdmin();
				//Admin peut plus jouer 
				((MainMenuPanel)parent.getJpMainMenu()).showAdminBoutton();
			}else{
				/*Mot de passe incorrect*/
				message = "Invalid password";
				getJlMessagePassword().setText(message);
			}
		} catch (NullPointerException e) {
			/*Nom d'utilisateur incorrect*/
			message ="Invalid username";
			getJlMessageUsername().setText(message);
		}

		getJlMessagePassword().repaint();
		getJlMessageUsername().repaint();
	}
	public JButton getJbGuest() {
		if(jbGuest == null){
			jbGuest = new TexturedJButton("Play as guest","stone.png");
			jbGuest.setForeground(Color.WHITE);
			jbGuest.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					playGuest();
				}

			});
		}
		return jbGuest; 
	}
	/**
	 * it allows to go the subscribe panel
	 */
	private void subscribe(){
		CardPanel parent = (CardPanel) this.getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.next(parent);
	}

	private void playGuest(){
		resetMessage();
		CardPanel parent = (CardPanel) this.getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		parent.setPlayer(GestionPersonne.getInstance().getUser("guest"));
		card.show(parent,"Main Menu");
		((OptionPanel)parent.getJpOption()).showAdmin();
	}
	/**
	 * it allows to reset the JLabel
	 */
	private void resetMessage(){
		getJlMessagePassword().setText("");
		getJlMessagePassword().repaint();
		getJlMessageUsername().setText("");
		getJlMessageUsername().repaint();
	}
	private void changeTitle(JLabel jl){
		Font customFont=null;
		try {
			customFont = Font.createFont(Font.TRUETYPE_FONT, new File("Gameplay.ttf")).deriveFont(10f);
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		//register the font
		try {
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("Gameplay.ttf")));
		} catch (FontFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jl.setFont(customFont);
		jl.setForeground(Color.WHITE);
	}

}