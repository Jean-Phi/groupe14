package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Collections;
import java.util.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import models.Deck;
import models.TexturedJButton;
import models.TexturedJLabel;
import utilities.Recognition;

public class ThemeSelectionPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton  jbBack;
	private TexturedJButton jbTheme1, jbTheme2, jbTheme3, jbTheme4;
	private JLabel jlTitle, jlDifficulty;
	private ImageIcon previousImage;
	/**
	 * It allows to position differents elements on the panel
	 */
	public ThemeSelectionPanel(){
		GroupLayout gl=new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);

		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJlTitle(),150,150,150)
						.addGap(400)
						.addComponent(getJlDifficulty(), 150, 150, 150)
						)
				.addGap(20)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbTheme1(),200,200,200)
						.addComponent(getJbTheme2(),200,200,200)
						.addComponent(getJbTheme3(),200,200,200)
						.addComponent(getJbTheme4(),200,200,200)
						)
				.addGap(20)
				.addComponent(getJbBack(),140,140,140)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addGroup(gl.createParallelGroup()
						.addComponent(getJlTitle(),40,40,40)
						.addComponent(getJlDifficulty(),40,40,40)
						)
				.addGap(20)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbTheme1(),80,80,80)
						.addComponent(getJbTheme3(),80,80,80)
						)
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbTheme2(),80,80,80)
						.addComponent(getJbTheme4(),80,80,80)
						)
				.addGap(100)
				.addComponent(getJbBack(),40,40,40)
				);
	}
	/**
	 * create a JLabel
	 * @return JLabelTitle
	 */
	public JLabel getJlTitle() {
		if(jlTitle==null){
			jlTitle=new TexturedJLabel("Choose your theme !","Parchemin.png");
		}
		return jlTitle;
	}
	/**
	 * create a JButton wich allows you to select a theme
	 * @return FormatedButton
	 */
	public TexturedJButton getJbTheme1() {
		if(jbTheme1==null){
			jbTheme1=new TexturedJButton("Theme 1","parcho.png");
			hideButton(jbTheme1);
			jbTheme1.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					selection(arg0);
				}

			});
		}
		return jbTheme1;
	}
	/**
	 * create a JButton wich allows you to select a theme
	 * @return JButton
	 */
	public TexturedJButton getJbTheme2() {
		if(jbTheme2==null){
			jbTheme2=new TexturedJButton("Theme 2","parcho.png");
			hideButton(jbTheme2);
			jbTheme2.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					selection(arg0);
				}

			});
		}
		return jbTheme2;
	}
	/**
	 * create a JButton wich allows you to select a theme
	 * @return JButton
	 */
	public TexturedJButton getJbTheme3() {
		if(jbTheme3==null){
			jbTheme3=new TexturedJButton("Theme 3","parcho.png");
			hideButton(jbTheme3);
			jbTheme3.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					selection(arg0);	
				}

			});
		}
		return jbTheme3;
	}
	/**
	 *  create a JButton wich allows you to select a theme
	 * @return JButton
	 */
	public TexturedJButton getJbTheme4() {
		if(jbTheme4==null){
			jbTheme4=new TexturedJButton("Theme 4","parcho.png");
			hideButton(jbTheme4);
			jbTheme4.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					selection(arg0);
				}

			});
		}
		return jbTheme4;
	}
	/**
	 * create a button wich allows you to go to the previous panel
	 * @return JButton
	 */
	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","stone.png");
			jbBack.setForeground(Color.white);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					back();
				}
			});
		}
		return jbBack;
	}

	public JLabel getJlDifficulty(){
		if(jlDifficulty == null){
			jlDifficulty = new TexturedJLabel("Difficulty : "+Recognition.getDifficult().getName(),"Parchemin.png");
		}
		return jlDifficulty;
	}

	/**
	 * it allows you to on the main menu
	 */
	private void back(){
		CardPanel parent = (CardPanel) getParent().getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Main Menu");
	}

	/**
	 * Select the theme and show next panel with the theme selected
	 * @param arg0 ActionEvent
	 */
	private void selection(ActionEvent arg0) {
		int i;
		SinglePlayerCardPanel parent = (SinglePlayerCardPanel) this.getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.next(parent);
		JButton o = (JButton) arg0.getSource();
		GamePanel gp = (GamePanel) parent.getJpGamePanel();
		if(!o.getText().equals("?")){
			gp.start(o.getText());
		}else{
			List<String> themes = Deck.getInstance().getThemes();
			Collections.shuffle(themes);
			gp.start(themes.get(0));
		}
		for(i=1;i<=10;i++){
			ImageIcon image1= new ImageIcon(i-1+"point.gif");
			swapImage(image1);
		}
	}
	/**
	 * it hide the buttons
	 * @param jb
	 */
	private void hideButton(JButton jb){
		jb.setOpaque(false);
		jb.setContentAreaFilled(false);
		jb.setBorderPainted(false);
	}

	/**
	 * reset the text of all buttons with a random theme
	 */
	public void resetThemes(){
		List<String> themes = Deck.getInstance().getThemes();
		Collections.shuffle(themes);
		getJbTheme1().setText(themes.get(0));
		getJbTheme2().setText(themes.get(1));
		getJbTheme3().setText(themes.get(2));
		getJbTheme4().setText("?");
		getJlDifficulty().setText("Difficulty : "+Recognition.getDifficult().getName());
	}
	/**
	 * it makes the background
	 */
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("cordes.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
	public void swapImage(ImageIcon image) {
		setPreviousImage(null);
		image.getImage().flush();
		setPreviousImage(image);
	}
	public ImageIcon getPreviousImage() {
		return previousImage;
	}
	public void setPreviousImage(ImageIcon previousImage) {
		this.previousImage = previousImage;
	}
}
