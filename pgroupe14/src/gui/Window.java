package gui;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import models.Audio;
import models.Deck;
import models.GestionPersonne;
import models.HighScore;
import utilities.Synonym;

public class Window extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel jpCards;
	private Audio audio;
	
	/**
	 * Constructor window
	 */
	public Window(){
		Deck.getInstance().fromJson("Questions.json");
		HighScore.getInstance().fromJson("scores.json");
		Synonym.getInstance().fromJson("data_synonyms.json");
		GestionPersonne.getInstance().fromJson("Users.json");
		this.setTitle("Climbing Questions");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(800, 450);
		this.setResizable(false);
		this.add(getJpCards());
		this.setLocationRelativeTo(null);
		this.setContentPane(jpCards);
		this.setIconImage(new ImageIcon("GameIcon.png").getImage());
	}

	/**
	 * it creates a JPanel
	 * @return
	 * return a Jlabel jbSave
	 *
	 */
	public JPanel getJpCards() {
		if(jpCards == null){
			jpCards = new CardPanel();
		}
		return jpCards;
	}
	
	/**
	 * it creates an Audio
	 * @return
	 * return a sound Files
	 *
	 */
	public Audio getAudio() {
		if(audio==null){
			audio=new Audio("Pim Poy.wav");
		}
		return audio;
	}
	
	/**
	 * it allows to set the Audio
	 * @param audio
	 */
	public void setAudio(Audio audio) {
		this.audio = audio;
	}
	
	/**
	 * it allows to make the window visible
	 * with this function the sound in the same time that the window
	 */
	@Override
	public void setVisible(boolean arg0) {
		super.setVisible(arg0);
		if(arg0){
			getAudio().jouerEnBoucle();
		}else{
			getAudio().arreter();
		}
	}	
	/**
	 * Main method
	 */
	public static void main(String[]args){
		new Window().setVisible(true);
	}
}