package gui;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

import models.User;

public class CardPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel  jpLogin, jpSinglePlayer, jpMainMenu, jpMultiplayer, jpScoreboard, jpSubscribe, jpAdmin, jpOption;
	private User player;
	
	/**
	 * Constructor of Cardpanel
	 */
	
	public CardPanel() {
		super();
		player = null;
		this.setPreferredSize(new Dimension(800,500));
		this.setLayout(new CardLayout());
		this.add(getJpLogin(), "LoginPanel");
		this.add(getJpSubscribe(), "Subscribe");
		this.add(getJpMainMenu(),"Main Menu");
		this.add(getJpSinglePlayer(),"Single Player");
		this.add(getJpMultiplayer(),"Multiplayer");
		this.add(getJpScoreboard(),"Scoreboard");
		this.add(getJpOption(), "Option");
		this.add(getJpAdminOption(), "Admin Option");
		this.setSize(800, 450);
	}
	/**
	 * It allows to create the AdminOption
	 * @return
	 * Return the panel of the AdminCardPanel
	 */
	public JPanel getJpAdminOption() {
		if(jpAdmin == null){
			jpAdmin = new AdminCardPanel();
		}
		return jpAdmin;
	}

	public JPanel getJpSubscribe() {
		if(jpSubscribe==null){
			jpSubscribe = new SubscribePanel();
		}
		return jpSubscribe;
	}
	/**
	 * It allows to create the MainMenuPanel
	 * @return
	 * Return the panel of the MainMenu
	 */
	public JPanel getJpMainMenu() {
		if(jpMainMenu == null){
			jpMainMenu = new MainMenuPanel();
		}
		return jpMainMenu;
	}
	/**
	 * It allows to create the LoginPanel
	 * @return
	 * Return the panel of the LoginPanel
	 */
	public JPanel getJpLogin() {
		if(jpLogin == null){
			jpLogin = new LoginPanel();
		}
		return jpLogin;
	}
	/**
	 * It allows to create the SinglePlayerCardPanel
	 * @return
	 * Return the panel of the SinglePlayerCardPanel
	 */
	public JPanel getJpSinglePlayer() {
		if(jpSinglePlayer == null){
			jpSinglePlayer = new SinglePlayerCardPanel();
		}
		return jpSinglePlayer;
	}
	/**
	 * It allows to create the MultiplayerPanel
	 * @return
	 * Return the panel of the MultiplayerPanel
	 */
	public JPanel getJpMultiplayer() {
		if(jpMultiplayer == null){
			jpMultiplayer = new MultiplayerPanel();
		}
		return jpMultiplayer;
	}
	/**
	 * It allows to create the ScoreboardPanel
	 * @return
	 * Return the panel of the ScoreboardPanel
	 */
	public JPanel getJpScoreboard() {
		if(jpScoreboard == null){
			jpScoreboard = new ScoreboardPanel();
		}
		return jpScoreboard;
	}
	/**
	 * It allows to create the OptionPanel
	 * @return
	 * Return the panel of the OptionPanel
	 */
	public JPanel getJpOption() {
		if(jpOption == null){
			jpOption = new OptionPanel();
		}
		return jpOption;
	}

	/**
	 * 
	 * @return The player
	 */
	public User getPlayer() {
		return player;
	}

	/**
	 * 
	 * @param user
	 * It allows to modify the user
	 */
	public void setPlayer(User user) {
		player = user;
	}
	/** 
	 * it makes a background 
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("background.gif").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
}
