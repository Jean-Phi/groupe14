package gui;

import java.awt.CardLayout;
import java.awt.Dimension;
import javax.swing.JPanel;

public class AdminCardPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JPanel jpAdminOption, jpDeck, jpUsers, jpUserCreation, jpQuestionCreation, jpSynonym, jpSynonymCreation;
	/**
	 * CardPanel construction
	 */

	public AdminCardPanel(){
		this.setPreferredSize(new Dimension(800,500));
		this.setLayout(new CardLayout());
		this.add(getJpAdminOption(), "Admin option");
		this.add(getJpDeck(), "Deck");
		this.add(getJpQuestionCreation(), "Create question");
		this.add(getJpUsers(), "Users");
		this.add(getJpUserCreation(),"Create user");
		this.add(getJpSynonym(), "Synonym");
		this.add(getJpSynonymCreation(), "Create synonym");
		this.setSize(800, 450);
	}
	/**
	 * It allows to create an AdminOptionPanel
	 * @return
	 * Return the panel of the AdminOptionPanel
	 */
	public JPanel getJpAdminOption() {
		if(jpAdminOption == null){
			jpAdminOption = new AdminOptionPanel();
		}
		return jpAdminOption;
	}
	/**
	 * It allows to create the DeckTablePanel
	 * @return
	 * Return the panel of the DeckTablePanel
	 */
	public JPanel getJpDeck() {
		if(jpDeck == null){
			jpDeck = new DeckTablePanel();
		}
		return jpDeck;
	}
	/**
	 * It allows to create the UserTablePanel
	 * @return
	 * Return the panel of the UserTablePanel
	 */
	public JPanel getJpUsers() {
		if(jpUsers == null){
			jpUsers = new UsersTablePanel();
		}
		return jpUsers;
	}
	/**
	 * It allows to create the UserCreationPanel
	 * @return
	 * Return the panel of the UserCreationPanel
	 */
	public JPanel getJpUserCreation() {
		if(jpUserCreation == null){
			jpUserCreation = new UserCreationPanel();
		}
		return jpUserCreation;
	}
	/**
	 * It allows to create the QuestionCreationPanel
	 * @return
	 * Return the panel of the QuestionCreationPanel
	 */
	public JPanel getJpQuestionCreation() {
		if(jpQuestionCreation == null){
			jpQuestionCreation = new QuestionCreationPanel();
		}
		return jpQuestionCreation;
	}
	public JPanel getJpSynonym() {
		if(jpSynonym == null){
			jpSynonym = new SynonymTablePanel();
		}
		return jpSynonym;
	}
	
	public JPanel getJpSynonymCreation() {
		if(jpSynonymCreation == null){
			jpSynonymCreation = new SynonymCreationPanel();
		}
		return jpSynonymCreation;
	}
	
}
