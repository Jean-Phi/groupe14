package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import enumerations.Difficulty;
import models.HighScore;
import models.Observeur;
import models.Score;
import models.TexturedJButton;

class ScoreboardTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String [] nomColonnes = {"Nickname", "Theme", "Score","Difficulty"};
	private Difficulty difficulty = Difficulty.ALL;
	private int rowCount = 0;

	public void setRowCount(){
		if(difficulty.equals(Difficulty.ALL)){
			rowCount = HighScore.getInstance().size();
		}else{
			rowCount = HighScore.getInstance().getScoresDifficulty(difficulty).size();
		}
	}
	public void setChoice(Difficulty difficulty){
		this.difficulty = difficulty;
		setRowCount();
	}
	@Override
	public int getColumnCount() {
		return 4;
	}

	public void addRow(Object o){
		this.addRow(o);
	}

	@Override
	public int getRowCount() {
		setRowCount();
		return rowCount;
	}

	public Object getValueAt(int ligne) {
		Score score=null;
		if(difficulty.equals(Difficulty.ALL)){
			score = HighScore.getInstance().getScore(ligne);
		}else{
			score = HighScore.getInstance().getScoresDifficulty(difficulty).get(ligne);
		}
		return score;
	}
	@Override
	public Object getValueAt(int ligne, int colonne) {
		Score score=null;
		if(difficulty.equals(Difficulty.ALL)){
			score = HighScore.getInstance().getScore(ligne);
		}else{
			score = HighScore.getInstance().getScoresDifficulty(difficulty).get(ligne);
		}
		String result ="";
		switch(colonne){
		case 0: result = score.getNickname(); break;
		case 1 : result = score.getTheme(); break;
		case 2 : result =""+score.getScore(); break;
		case 3 : result =""+score.getDiff(); break;
		}
		return result;
	}

	@Override
	public String getColumnName(int column) {
		return nomColonnes [column];
	}
}

public class ScoreboardPanel extends JPanel implements Observeur{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle;
	private JButton jbBack;
	private JTable jtable;
	private JScrollPane jsTable;
	private JComboBox<Difficulty> jcDifficulties;

	public ScoreboardPanel(){
		HighScore.getInstance().ajoute(this);
		GroupLayout gl=new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);

		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(800))
				.addComponent(getJlTitle())
				.addComponent(getJcDifficulties(),700,700,700)
				.addComponent(getJsTable(),700,700,700)
				.addComponent(getJbBack(),200,200,200)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addComponent(getJlTitle())
				.addComponent(getJcDifficulties())
				.addComponent(getJsTable())
				.addComponent(getJbBack(),40,40,40)
				);
	}

	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new JLabel("Scoreboard");
		}
		return jlTitle;
	}

	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","stone.png");
			jbBack.setForeground(Color.WHITE);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					back();
				}
			});
		}
		return jbBack;
	}

	public JScrollPane getJsTable() {
		if(jsTable == null){
			jsTable = new JScrollPane(getJtable());
		}
		return jsTable;
	}

	public JTable getJtable() {
		if(jtable == null){
			jtable = new JTable(new ScoreboardTableModel());
		}
		return jtable;
	}
	
	public JComboBox<Difficulty> getJcDifficulties() {
		if(jcDifficulties == null){
			Difficulty arg0[] = {Difficulty.EASY, Difficulty.MEDIUM, Difficulty.HARD, Difficulty.EXPERT};
			jcDifficulties = new JComboBox<>(arg0);
			jcDifficulties.addItem(Difficulty.ALL);
			jcDifficulties.setSelectedItem(Difficulty.ALL);
			jcDifficulties.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					ScoreboardTableModel mt = (ScoreboardTableModel)getJtable().getModel();
					mt.setChoice((Difficulty)getJcDifficulties().getSelectedItem());
					getJsTable().repaint();
				}
			});
		}
		return jcDifficulties;
	}

	private void back(){
		CardPanel parent = (CardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Main Menu");
	}
	public void actualise() {
		ScoreboardTableModel mt = (ScoreboardTableModel) getJtable().getModel();
		mt.fireTableDataChanged();
		mt.fireTableRowsDeleted(0, mt.getRowCount());
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("scoreboardBG.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
}
