package gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import models.Observeur;
import models.TexturedJButton;
import utilities.Synonym;

import javax.swing.table.AbstractTableModel;


class SynonymTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String [] nomColonnes = {"Source","Synonyms"};
	private int rowCount = Synonym.getInstance().size();

	public void setRowCount(){
			rowCount = Synonym.getInstance().size();
	}

	@Override
	public int getColumnCount() {
		return nomColonnes.length;
	}

	public void addRow(Object o){
		this.addRow(o);
	}

	@Override
	public int getRowCount() {
		return rowCount;
	}

	public Object getValueAt(int ligne){
		return Synonym.getInstance().get(ligne);
	}

	@Override
	public Object getValueAt(int ligne, int colonne) {
		setRowCount();
		switch(colonne){
		case 0 : return Synonym.getInstance().get(ligne).get(colonne);
		case 1 : return Synonym.getInstance().get(ligne);
		}
		return colonne;
	}

	@Override
	public String getColumnName(int column) {
		return nomColonnes [column];
	}

	
	public boolean isCellEditable(int arg0, int arg1) {
		return false;
	}
}

/**
 * 
 * Classe panel
 *
 */


public class SynonymTablePanel extends JPanel implements Observeur{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JLabel jlTitle;
	private JButton jbBack, jbDelete, jbCreation, jbUndo, jbEdit;
	private JTable jtable;
	private JScrollPane jsTable;

	public SynonymTablePanel (){
		Synonym.getInstance().ajoute(this);
		GroupLayout gl=new GroupLayout(this);
		this.setLayout(gl);
		gl.setAutoCreateContainerGaps(true);
		gl.setAutoCreateGaps(true);

		//GROUP HORIZONTAL
		gl.setHorizontalGroup(gl.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addGroup(gl.createSequentialGroup().addGap(700))
				.addComponent(getJlTitle())
				.addComponent(getJsTable(),765,765,765)
				.addGroup(gl.createSequentialGroup()
						.addComponent(getJbBack())
						.addComponent(getJbUndo())
						.addComponent(getJbDelete())
						.addComponent(getJbEdit())
						.addComponent(getJbCreation())
						)
				);

		//GROUP VERTICAL
		gl.setVerticalGroup(gl.createSequentialGroup()
				.addComponent(getJlTitle())
				.addComponent(getJsTable())
				.addGroup(gl.createParallelGroup()
						.addComponent(getJbBack(),40,40,40)
						.addComponent(getJbUndo(),40,40,40)
						.addComponent(getJbDelete(),40,40,40)
						.addComponent(getJbEdit(),40,40,40)
						.addComponent(getJbCreation(),40,40,40)
						)
				);
	}

	public JLabel getJlTitle() {
		if(jlTitle == null){
			jlTitle = new JLabel("Synonym");
			jlTitle.setForeground(Color.white);
		}
		return jlTitle;
	}

	public JButton getJbBack() {
		if(jbBack == null){
			jbBack = new TexturedJButton("Back","stone.png");
			jbBack.setForeground(Color.WHITE);
			jbBack.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					back();
				}
				
			});
		}
		return jbBack;
	}

	public JScrollPane getJsTable() {
		if(jsTable == null){
			jsTable = new JScrollPane(getJtable());
		}
		return jsTable;
	}

	public JTable getJtable() {
		if(jtable == null){
			jtable = new JTable(new SynonymTableModel());
		}
		return jtable;
	}


	public JButton getJbDelete() {
		if(jbDelete == null){
			jbDelete = new TexturedJButton("Delete","stone.png");
			jbDelete.setForeground(Color.white);
			jbDelete.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					int rows[] = getJtable().getSelectedRows();
					SynonymTableModel mt = (SynonymTableModel)getJtable().getModel();
					for(int i = rows.length-1; i>=0; i--){
						Synonym.getInstance().removeSynonym(rows[i]);
					}
					Synonym.getInstance().toJson("data_synonyms.json");
					mt.setRowCount();
				}
			});
		}
		return jbDelete;
	}

	public JButton getJbCreation() {
		if(jbCreation == null){
			jbCreation = new TexturedJButton("Create synonym","stone.png");
			jbCreation.setForeground(Color.WHITE);
			jbCreation.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					JButton o = (JButton) e.getSource();
					AdminCardPanel parent = (AdminCardPanel) getParent();
					CardLayout card = (CardLayout) parent.getLayout();
					card.show(parent, o.getText());
					((SynonymCreationPanel) parent.getJpSynonymCreation()).init();
				}
			});
		}
		return jbCreation;
	}

	public JButton getJbUndo() {
		if(jbUndo == null){
			jbUndo = new TexturedJButton("Undo","stone.png");
			jbUndo.setForeground(Color.white);
			jbUndo.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent arg0) {
					Synonym.undo();
				}
			});
		}
		return jbUndo;
	}

	public JButton getJbEdit() {
		if(jbEdit == null){
			jbEdit = new TexturedJButton("Edit","stone.png");
			jbEdit.setForeground(Color.white);
			jbEdit.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					edit();
					Synonym.getInstance().toJson("data_synonyms.json");
				}
			});
		}
		return jbEdit;
	}
	
	private void back(){
		AdminCardPanel parent = (AdminCardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent, "Admin option");
	}

	public void edit(){
		AdminCardPanel parent = (AdminCardPanel) getParent();
		CardLayout card = (CardLayout) parent.getLayout();
		card.show(parent,"Create synonym");
		int index = getJtable().getSelectedRow();
		List<String> l = Synonym.getInstance().get(index);
		((SynonymCreationPanel) parent.getJpSynonymCreation()).editSynonym(l);
	}
	
	@Override
	public void actualise() {
		SynonymTableModel mt = (SynonymTableModel) getJtable().getModel();
		mt.setRowCount();
		mt.fireTableDataChanged();
		mt.fireTableRowsDeleted(0, mt.getRowCount());
	}
	
	public void init(){
		actualise();
		getJtable().repaint();
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Image image = new ImageIcon("scoreboardBG.png").getImage();
		g.drawImage(image,0,0,getWidth(),getHeight(),this);
	}
	
}
